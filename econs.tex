

\section{Energy Consumption on the \\ TEGRA X1 SoC}

The facilitation of an accurate power model for the Tegra X1 SoC mandates deep
knowledge about how the platform consumes energy in terms of static and dynamic
power components. In this section, we investigate the effects of sub-threshold
and gate-oxide transistor leakage, and outline the dynamic power model
predictors used in our model. Not considering the leakage effects, our outline
for this section will generally follow the same structure as in our previous work on the
Tegra~K1~\cite{Stokke2016mmsys,Stokke2016mcsoc}. However, the discussion in this
paper features updated discussion around which HPCs to use for modelling
dynamic power on the \tegra, due to architectural changes and new hardware
features. 

\subsection{Effects of Sub-Threshold \\ Transistor Leakage}
\label{sec:subthreffects}

Sub-threshold transistor leakage was recognised as one of the two most substantial
sources of transistor leakage by the ITRS and research community in
2003~\cite{Kim2003,Roy2003}, due to the continued reduction of transistor supply voltage
and feature size. This type of leakage is \textit{temperature-dependent} and occurs when the transistor is in its
\textit{off} state (operating below $V_{th}$), when ideally, no current should
flow between the source and drain terminals. Sub-threshold leakage current is
described according to the following equation~\cite{Kim2003}:

\begin{equation}
    I_{sub} (V, V_0) = K_1 W e^{\frac{-V_{th}}{nV_0}}(1 - e^{\frac{-V}{V_0}})
    \label{equ:subthr}
\end{equation}

In Equation~\ref{equ:subthr}, $K_1$ and $n$ are experimentally derived
parameters, $W$ is the gate width, $V$ is the source-drain voltage potential,
$V_{th}$ is the threshold voltage and $V_0$ is the thermal voltage. According to
Kim, S et. al.~\cite{Kim2003}, $V_0$ can be described as a linear function of
temperature, where the thermal voltage is $25 mV$ at room temperature ($V_0
[T = 20 \,^{\circ}\mathrm{C}] = 25 mV$):

\begin{equation}
    V_0 (T) = \alpha (T - 20) + 25 mV
    \label{equ:thermvolt}
\end{equation}

\begin{figure*}
    \begin{center}
        \begin{subfigure}[t]{.30\textwidth}
        \includegraphics[scale=0.09, trim = 2cm 0cm 0cm 0cm, clip = true]{gpuleakinit.pdf}
            \caption{\label{fig:gpuleakinit}GPU sub-threshold.}
        \end{subfigure}
        \begin{subfigure}[t]{.30\textwidth}
        \includegraphics[scale=0.09, trim = 2cm 0cm 0cm 0cm, clip = true]{coreleakinit.pdf}
            \caption{\label{fig:coreleakinit}Core sub-threshold.}
        \end{subfigure}
        \begin{subfigure}[t]{.30\textwidth}
        \includegraphics[scale=0.09, trim = 2cm 0cm 0cm 0cm, clip = true]{hpcpuleakinit.pdf}
            \caption{\label{fig:hpcpuleakinit}HP sub-threshold.}
        \end{subfigure}
    \end{center}

    \begin{center}
        \begin{subfigure}[t]{.30\textwidth}
        \includegraphics[scale=0.09, trim = 2cm 0cm 0cm 0cm, clip = true]{gpuleakinitgo.pdf}
            \caption{\label{fig:gpuleakinitgo}GPU gate-oxide.}
        \end{subfigure}
        \begin{subfigure}[t]{.30\textwidth}
        \includegraphics[scale=0.09, trim = 2cm 0cm 0cm 0cm, clip = true]{coreleakinitgo.pdf}
            \caption{\label{fig:coreleakinitgo}Core gate-oxide.}
        \end{subfigure}
        \begin{subfigure}[t]{.30\textwidth}
        \includegraphics[scale=0.09, trim = 2cm 0cm 0cm 0cm, clip = true]{hpcpuleakinitgo.pdf}
            \caption{\label{fig:hpcpuleakinitgo}HP gate-oxide.}
        \end{subfigure}
    \end{center}

    \caption{\label{fig:leakinit}\jetson rate of energy consumption plotted oved average chip
    temperature and rail voltage intervals.}
%    \vspace{-2.3cm}
\end{figure*}

In Equation~\ref{equ:thermvolt}, $\alpha$ is an unknown coefficient that can be
viewed as a parameter describing how susceptible the transistor is to increased temperature. $W$ and
$K_1$ from Equation~\ref{equ:subthr} are also unknown, and are viewed as
a single coefficient $\beta$ that represents the transistor count and gate width. 
We ignore $n$ for the remainder of this paper. 

The scope of estimating $\beta$ and $\alpha$ is not trivial. It is essentially
necessary to vary both chip temperature and transistor supply voltage in a way
that maximises the contribution to sub-threshold leakage, and avoids
contributing to other power components, such as dynamic clock power. This task
is non-trivial and impossible to do on one rail at a time. Increasing chip 
temperature will for example not only increase
leakage on a particular rail. Temperature variations propagate through all the
components of the SoC, affecting leakage current across all power rails
at the same time. Furthermore, increasing supply voltage on a rail will
always increase dynamic power usage on that rail ($P_{dyn} \propto V^2$). Additionally,
this is usually done by scaling up clocks drawing power
from that supply rail, which we want to avoid. However, on the \tegra, this last
point can be avoided on the GPU and Core rails, where it is possible to
statically assign rail voltage manually. This does not work on the CPU HP rail.

To study the overall effect that chip temperature $T$ and supply voltage $V$ has on
total power usage of the \jetson, we ignore temperature variation across the
\tegra's rails and perform the following experiment with the \tegra's GPU:

\begin{itemize}
    \item We first remove the entire cooling solution from the development kit, exposing the SoC to free air. 
    \item Boot the system and set all clocks on the \tegra to static values, minimising the GPU clock frequency. 
    \item The \tegra is now allowed to idle while collecting all sensor data (see Section~\ref{sec:sensor}) for ten minutes.
        \begin{itemize}
            \item In this period, we run a script that continuously varies GPU
                rail voltage in units of 20 mV $\in [0.80 V, 1.05 V]$.
            \item We also vary chip temperature by manually heating up and
                cooling down the SoC for the duration of the test.
        \end{itemize}
\end{itemize}

The measured \jetson power usage versus average chip temperature is shown in the upper row
of Figure~\ref{fig:leakinit} for the three main power rails of the \tegra. For each rail,
we divide the dataset into five bins of voltage ranges to study how platform power usage
varies with temperature. In all voltage ranges of Figures~\ref{fig:gpuleakinit},
\ref{fig:coreleakinit} and \ref{fig:hpcpuleakinit}, there is a strong correlation between the
average chip temperature and total platform power usage. This is illustrated  by modelling
the unknown variables of Equation~\ref{equ:subthr} in each voltage bin using the
non-linear least squares method, and use the resulting model to draw an estimated total
power usage over the same temperature range and voltage region. In general, the higher the
voltage, the higher the achieved power usage ($P_{stat} \propto I_{leak}$). The resulting
curves show decent fits with the experimental data, indicating that the sub-threshold
leakage Equation \ref{equ:subthr} is well suited to model static power usage in the given
voltage ranges. We will study our resulting sub-threshold leakage model in
Section. 

\subsection{Effects of Gate-Oxide Transistor Leakage}

Gate-oxide transistor leakage, like sub-threshold transistor leakage, occurs when the
transistor is off. It is caused by quantum-tunneling effects as the transistor's
di-electric layer thickness decreases, where small currents propagate from the
source-drain channel to the gate. Kim, S. et. al.~\cite{Kim2003} describe gate-oxide
leakage as follows:

\begin{equation}
    I_{ox} (V) = K_2 W(\frac{V}{T_{ox}})^2 e^{\frac{-\alpha T_{ox}}{V}}
    \label{equ:gateox}
\end{equation}

In Equation~\ref{equ:gateox}, $K_2$ and $\alpha$ are experimentally derived parameters,
and $T_{ox}$ is the oxide thickness. The only variable parameter is the transistor voltage
$V$.

To study the effects of gate-oxide leakage, we now use the experimental data from
Section~\ref{sec:subthreffects} and plot the measured platform power usage over the three
rail voltages in Figures~\ref{fig:gpuleakinitgo}, \ref{fig:coreleakinitgo} and
\ref{fig:hpcpuleakinitgo}. Gate-oxide leakage is not temperature dependent. However, as
increased temperature will be evident in higher total power usage caused by sub-threshold
leakage, we categorise the data into five bins depending on the temperature range. The
core idea behind this is to isolate the effects that transistor voltage has on total power
usage. Because oxide leakage is proportional to the square rail voltage ($I_{ox} \propto
V^2$) we expect to notice this trend in the experimental data. However, by studying
Figures~\ref{fig:gpuleakinitgo}, \ref{fig:coreleakinitgo} and \ref{fig:hpcpuleakinitgo}
the relation between total power usage and rail voltage is more linear in nature. However,
some also exhibit more variation. For example, the relation to GPU voltage in
Figure~\ref{fig:gpuleakinitgo} for the two lower temperature bins is linear, but the
high-temperature bin (75 C - 89 C) shows a tendency to start increasing superlinearly for
voltages above 0.9~V. However, Figure~\ref{fig:coreleakinitgo} and \ref{fig:hpcpuleakinitgo}, 
also confirm the lack of a polynomial relation between power usage and voltage. 

\subsection{Sub-Threshold Leakage Under Heavy Processing}

\begin{figure*}
    \begin{center}

        \begin{subfigure}[t]{.45\textwidth}
        \includegraphics[scale=0.13, trim = 0cm 0cm 0cm 0cm, clip = true]{gpuleakall.pdf}
            \caption{\label{fig:gpuleakall}GPU. ($V_{gpu} \in [0.80 V, 0.86 V]$)}
        \end{subfigure}
        \begin{subfigure}[t]{.45\textwidth}
        \includegraphics[scale=0.13, trim = 0cm 0cm 0cm 0cm, clip = true]{cpuleakall.pdf}
            \caption{\label{fig:cpuleakall}CPU ($V_{cpu} \in [0.98 V, 1.00 V]$).}
        \end{subfigure}
    \end{center}

    \caption{\label{fig:procsub}The typical sub-threshold leakage curve shown for
    different, continuous processing scenarios.}
%    \vspace{-2.3cm}
\end{figure*}

\subsection{GPU}
\label{sec:gpudyn}

The \tegra features a 256-core Maxwell GPU, capable of both general purpose and graphics
processing~\cite{x1chip2015}. Compared to its predecessor, the Tegra X1, the
\tegra increases the number of CUDA cores by 64, distributed over two 128-core SMMs
(Streaming MultiProcessors) instead of only one SMX. According to Nvidia, the
128-core SMX is comparable in performance to the Tegra X1's 192-core SMM due to
improvements in execution efficiency. Additionally, the \tegra features
improvements to the memory hierarchy, most notably increased shared (L1) 64~kB cache
due to the combination of the Kepler's read-only and texture L1 cache, and
larger L2 cache (256~kB). The \tegra also features 16-bit floating point
hardware support, allowing for computational throughput of up to 1024~GFLOPs.
Hardware activity can be traced with CUPTI, a library that can be used to
measure GPU performance via HPCs. In this section, we outline how we use and
combine the \tegra's HPCs to measure hardware activity, used as predictors in
our dynamic power model.

\subsubsection{Cache Hierarchy}

The \tegra's two SMXs implement four warp schedulers each. Each warp scheduler
is capable of issuing a group of 32 threads called a \textit{warp} concurrently
with an isolated set of resources (such as CUDA cores) being allocated to each
warp. Due to the high number of concurrently active threads, DRAM memory pressure 
can become substantial. Therefore the \tegra features an efficient and highly
configurable cache hierarchy: one L2 cache shared between the SMXs,
a local L1 cache per SMX split into 64~kB of \textit{shared memory}, and 64~kB of
read-only L1 cache. Shared memory, which is used by groups of threads, is allocated
and used by the programmer as needed, and the L1 and L2 caches can be selectively 
used to cache DRAM accesses as needed by using assembly modifiers to load and
store instructions. 

The \tegra's L2 cache is split into two partitions. Read and write utilisation in each
partition can be traced with two CUPTI HPCs:
\begin{equation}
    \begin{split}
        \rho_{gpu,l2r} = l2\_p0\_read\_sector\_queries\ \\ +\ l2\_p1\_read\_sector\_queries
        \\
        \rho_{gpu,l2w} = l2\_p0\_write\_sector\_queries\ \\ +\ l2\_p1\_write\_sector\_queries
    \end{split}
\end{equation}

Hardware utilisation that results in actual L1 cache (read-only) reads can only
be tracked by subtracting the number of L1 cache misses from the number of L1
cache queries. Also, there are two counters for each SMX:
\begin{equation}
    \begin{split}
        N_{l1r,query} = tex0\_cache\_sector\_queries\ \\ +\ tex1\_cache\_sector\_queries\\
        N_{l1r,miss} = tex0\_cache\_sector\_misses\ \\ +\ tex1\_cache\_sector\_misses\\
        \rho_{gpu,l1r} = N_{l1r_query} - N_{l1r,miss}
    \end{split}
    \label{equ:l1cachepred}
\end{equation}

For shared memory, there are only two HPCs: \texttt{shared\_load\_transactions}
and \texttt{shared\_store\_transactions}. Interestingly, we can not attribute
a capacitive load to the number of shared load transactions, because this type
of transaction also increases the texture cache queries counter in
Equation~\ref{equ:l1cachepred}. This can suggest that physically, the read-only and
shared L1 caches are implemented on the same type and bank of transistors, or
that they share profiling resources. Instead, in our model, shared memory load
cost is captured as read-only L1 cache utilisation. For shared writes, the
following HPC exclusively counts accesses to this hardware unit:
\begin{equation}
    \rho_{gpu,shw} = shared\_store\_transactions
\end{equation}

\subsubsection{Functional Units}

As GPU threads process data, they will utilise various hardware units to load
and store data from and to RAM, perform arithmetic operations and conversions,
control flow and other. Utilisation in these units is trivially measured by the
following HPCs:

\begin{itemize}
    \item \texttt{inst\_integer}
    \item \texttt{inst\_fp\_[16/32/64]}
    \item \texttt{inst\_control}
    \item \texttt{inst\_bit\_convert}
    \item \texttt{inst\_misc}
\end{itemize}

Of these HPCs, the half-precision (16 bit) floating point operations HPC is new.
We have also tried to use the load and store instruction HPC in our model;
however, this HPC always produces a negative coefficient which is likely due to
all memory access hardware utilisation already being picked up by the memory
HPCs. Additionally, the GPU clock frequency is a natural dynamic power predictor for
kernels, represented by the \texttt{elapsed\_cycles\_sm} CUPTI HPC.

\subsection{CPU}
\label{sec:cpudyn}

The \tegra, like the Tegra~K1, features a dual CPU cluster with dedicated
high-performance and low-power CPU cores. The \tegra incorporates a quad-core ARM Cortex
A-57 as well as a quad-core ARM Cortex A-53 CPU cluster. However, the dedicated
low-power cluster of A-53s is disabled. Online sources, some from Nvidia
officials, state that this is due to incompatibility of libc due to differences
between in- and out-of-order execution of the different heterogeneous cores. For
this reason, in this paper, we can only focus on the A57s. Hardware utilisation
for the \tegra's CPU is measured with PERF, Linux' own generic HPC
implementation, and ARM's implementation-specific performance counters~\cite{a57trm}.

\subsubsection{Clock- and Power-Gating}

ARM Cortex A-57 cores implement clock- and power-gating. As the cores idle, they
will temporarily stop their own clocks to save dynamic power. If a core is idle for long
periods of time, that core can be shut down entirely, effectively removing both
static (leakage) and dynamic power. The core frequency is a natural dynamic
power predictor, and the exact number of elapsed core cycles can be measured
using perf and the \texttt{CC} event counter. 

On the \tegra, measuring the time
a core spends in a fully power-gated state is more challenging. For the
Tegra~K1, we implemented a tracing framework in the Linux kernel that measured
the time any CPU core spent in a power-gated state. However, core power-gating
on the \tegra breaks PERF counting. This issue can be bypassed by disabling
power-gating in the Linux kernel. Note that we are still able to power-gate CPU
cores by turning them off and on manually, which is necessary to model
subthreshold leakage contribution from a CPU core. 

\subsubsection{Instruction and Cache Power}

Instructions are natural predictors for power models. Ideally, the individual
types and the number of executed instructions should be counted, attributing
a cost for each, similarly to the work done by other authors\cite{Pricopi} and the \tegra's
GPU in preceeding sections. However, the CPU performance counter implementation
does not support this and there is instead a generic instruction counter
counting any instruction execution. Therefore, similarly to our previous
work~\cite{Stokke}, we will attribute individual CPU
instruction costs on a per-application basis. 

CPU cache utilisation is counted as activity between the L1 and L2 cache
hierarchies as follows:
\begin{equation}
    \rho_{cpu,l1l2} = cpu\_l1d\_rf + cpu\_l1i\_rf - cpu\_l2\_rf
    \label{equ:cpul1}
\end{equation}
The count from Equation~\ref{equ:cpul1} represents cache traffic between the L1
and L2 cache hierarchies. In addition to refills, cache writebacks can also be
measured. Generally, cache writebacks are accompanied by cache refills, making it
harder to separate and assign costs to these separate events. Our model
predictor therefore reflect the combined cost of a refill and writeback. Note,
however, that there are exceptions to this rule, for example in the time it
takes for the all cache lines to refill after a power-ungating of a CPU core.
Cache traffic between the L2 cache and RAM is not directly modelled with CPU
HPCs, but is instead captured by our RAM activity counter in the following
section.

\subsection{RAM}
\label{sec:ramdyn}

The \tegra is equipped with two LPDDR~4 RAM banks, and is running at 1.2~V. The
clock rate at which it is running is a natural model predictor. However, the
\tegra SoC contains an activity monitor that is able to count the number of
active cycles spent serving CPU and GPU memory requests. Previous
research~\cite{Stokke} has shown this cost to be similar independently of the
source of the request. We have therefore augmented the kernel driver to expose
active cycles to running applications. This hardware monitor is unique to
the Tegra family of SoCs. If a model must be built for SoCs this functionality is
not supported, RAM accesses can be approximated by other HPCs, such
as cache miss counters available in the CPU and GPU HPC implementations. 

