
\section{Method}

\subsection{Model Derivation}

Dynamic power usage of a rail $R \in \mathbb{R}$ can be described
as~\cite{StokkePhD}:
\begin{equation}
    P_{R,dyn} = \sum_{i=1}^{N_R} C_{R,i} \rho_{R,i} V_R^2
    \label{equ:dyn}
\end{equation}
In Equation~\ref{equ:dyn}, $N_R$ is the number of dynamic power coefficients in
that domain, $C_{R,i}$ is an unknown capacitive load (in coloumbs per volt) of
event number $i$, $\rho_{R,i}$ is the model predictor (in accesses per second),
and $V_R$ is the rail voltage. Static power is assumed to depend only on
subthreshold leakage (see Equation~\ref{equ:subthr} and \ref{equ:thermvolt}):
\begin{equation}
    P_{R,stat} = V_R I_{sub}
\end{equation}
The total power usage of the \jetson can then be described as:
\begin{equation}
    P_{jetson} = \sum^{R \in \mathbb{R}}(P_{R,dyn} + P_{R,stat}) + P_{base}
    \label{equ:total}
\end{equation}
In Equation~\ref{equ:total}, $P_{base}$ is the constant base power usage of the
\jetson. 

KRS: Add leakage formulas and explain why we need the "other" thermal coefficient, and not only the CPU and GPU. 

\subsection{Sensor Setup}
\label{sec:sensors}

\begin{figure}[t!]
    \centering
    \includegraphics[scale=0.65, trim = 1cm 12cm 7cm 12cm, clip = true]{measure}
    \caption{\label{fig:measure}Measurement setup.}
\end{figure}

The rail voltages on the \jetson platform plays a significant role for both
static and dynamic power. To some extent, these are measurable because the
required voltages are usually stored in the Linux kernel as static
voltage-frequency tables in the code. These are communicated to off-chip
regulators during frequency change and can sometimes be read out through the sysfs
kernel interface. This is up to the driver developers. In our earlier work on the Tegra~K1~\cite{StokkePhD},
we found that the reported sysfs voltage was significantly incorrect, due to the
behaviour of the buck-regulators supplying the rail. The actual rail voltage
also depends on the power draw on the rail. 

We therefore improve our
measurement setup as can be seen in Figure~\ref{fig:measure}, where three
Keithley K2110 multimeters are continuously reading out rail voltage directly
soldered onto the buck regulator output pins. The location of these pins on the \tegra
module is not publicly available; however it is possible to measure the output
voltages manually, comparing with the reported required rail voltages, as the
various platform frequencies are changed. A dedicated logger is reading the
reported rail voltages, as well as power measurement samples from the K2280S SMU
powering and measuring power draw of the \jetson, storing these to memory. The
\tegra starts, stops and download the power and voltage logs as required between
tests. Synchronisation is done as in previous
papers~\cite{Stokke2015mcsoc,Rice2010}. 

Finally, in order to measure temperature, we use embedded sensors in the
\tegra SoC~\cite{tx1trm}. The physical location of these is in the CPU, GPU, PLL
and memory I/O complexes, meant to guide frequency scaling algorithms are alert
software in case of overheating. These sensors are calibrated by the
manufacturer and their value can be read out directly from the sysfs kernel
interface. Ideally, the different sensors should be used to measure the unique
domain temperature, such as for the circuitry powered by the GPU and HP rails.
However, the actual physical location of each sensor is unknown, and it is not
given that the reported temperature is a good measure of the transistor
temperature in that part of the circuit. Therefore, we use an average over these
temperatures as input to our power model to predict subthreshold leakage.

\subsection{Model Training}
\label{sec:modtrain}

\begin{table*}[t!]
    \begin{scriptsize}
        \begin{center}
            \begin{tabular}{| c | c | c | c | c | c |} \hline
                Rail    & Predictor & Description                               & Coefficient & Value (Nano) & Value (X1) \\ \hline
            \multirow{4}{*}{CPU} 
                    & $V_{hp,core} $    & Temp.-dependent leakage coefficient   & $\omega_{hp}$     & $ 0.69 $ & $ 1.66 $ \\ \cline{2-6}
                    & $\rho_{hp,clk}$   & Active cycles (any core)              & $C_{hp,clk}$      & $ 977.50 \frac{pC}{V} $ & $ 1516.52 \frac{pC}{V} $ \\ \cline{2-6}
                    & $\rho_{hp,ipsa}$  & CPU instructions                      & $C_{com,ips}$     & $ 484.21 \frac{pC}{V} $ & $ 2.45 \frac{pC}{V}$ \\ \cline{2-6}
            \multirow{13}{*}{GPU} 
                    & $V_{gpu}   $      & Temp.dependent leakage coefficient    & $\omega_{gpu}$    & $ 1.80 $ & $ 2.56 $ \\ \cline{2-6}
                    & $\rho_{gpu,clk}$ & Total clock cycles                     & $C_{gpu,clk}$     & $ 1.64 \frac{nC}{V} $ & $ 2.17 \frac{nC}{V} $ \\ \cline{2-6}
                    & $\rho_{gpu,L2R}$  & L2 cache sector reads                 & $C_{gpu,L2R}$     & $ 97.48 \frac{nC}{V} $ & $ 46.66 \frac{nC}{V} $ \\ \cline{2-6}
                    & $\rho_{gpu,L2W}$  & L2 cache sector writes                & $C_{gpu,L2W}$     & $ 101.47 \frac{nC}{V} $ & $ 36.65 \frac{nC}{V} $\\ \cline{2-6}
                    & $\rho_{gpu,L1R}$  & L1 cache sector reads                 & $C_{gpu,L1R}$     & $ 109.56 \frac{pC}{V} $ & $ 18.91 \frac{pC}{V} $ \\ \cline{2-6}
                    & $\rho_{gpu,L1W}$  & L1 cache sector writes                & $C_{gpu,SHW}$     & $ 94.98 \frac{pC}{V} $ & $ 67.03 \frac{pC}{V} $ \\ \cline{2-6}
                    & $\rho_{gpu,INT}$  & Integer instructions                  & $C_{gpu,INT}$     & $ 31.78 \frac{pC}{V} $ & $ 54.10 \frac{pC}{V} $ \\ \cline{2-6}
                    & $\rho_{gpu,F16}$  & Float (16-bit) instructions           & $C_{gpu,F16}$     & $ 90.44 \frac{pC}{V} $ & $ 33.32 \frac{pC}{V} $ \\ \cline{2-6}
                    & $\rho_{gpu,F32}$  & Float (32-bit) instructions           & $C_{gpu,F32}$     & $ 120.46 \frac{pC}{V} $ & $ 107.80 \frac{pC}{V} $ \\ \cline{2-6}
                    & $\rho_{gpu,F64}$  & Float (64-bit) instructions           & $C_{gpu,F64}$     & $ 120.04 \frac{pC}{V} $ & $ 40.93 \frac{pC}{V} $ \\ \cline{2-6}
                    & $\rho_{gpu,CNV}$  & Conversion instructions               & $C_{gpu,CNV}$     & $ 2.98 \frac{pC}{V} $ & $ 119.60 \frac{pC}{V} $  \\ \cline{2-6}
                    & $\rho_{gpu,MSC}$  & Miscellaneous instructions            & $C_{gpu,MSC}$     & $ 108.09 \frac{pC}{V} $ & $ 118.96 \frac{pC}{V} $ \\ \cline{2-6}
                    & $\rho_{gpu,CTRL}$ & Control instructions                  & $C_{gpu,CTRL}$    & $ 83.82 \frac{pC}{V} $ & $ 154.71 \frac{pC}{V} $ \\ \hline
            \multirow{2}{*}{Memory}
                    & $\rho_{mem,clk}$      & Clock cycles                      & $C_{mem,clk}$     & $ 419.88 \frac{pC}{V} $ & $ 396.91 \frac{pC}{V} $  \\ \cline{2-6}
                    & $\rho_{mem,act}$      & Active memory cycles              & $C_{mem,cpu}$     & $ 1147.12 \frac{nC}{V} $ & $ 640.58 \frac{nC}{V} $  \\ \hline
            \multirow{2}{*}{Other}
                    & $T_{soc}   $          & Thermal coefficient               & $\alpha_{soc}$    & $ 9.72 10^{-3} $ & $ 11.33 10^{-3}$  \\ \cline{2-6}
                    &                       & Temp.-dependent leakage coefficient   & $\omega_{other}$     & $ 1.23 $ & $ 1.60 $ \\ \cline{2-6}
                    & $P_{base}$            & Base power                        & 1                 & $ 692.48 mW$ & $ 1276.17 mW$ \\ \hline
            \end{tabular}
            \caption{\label{tab:model}Estimated model coefficients.}
        \end{center}
%        \vspace{-0.7cm}
    \end{scriptsize}
\end{table*}

To produce a high-precision power model, our method is to collect training data where all
the dynamic power predictors from Section~\ref{sec:gpudyn}, \ref{sec:cpudyn} and
\ref{sec:ramdyn}, as well as rail voltages and chip temperature, is varied. The
resulting dataset is then input to a non-linear, least-squares regression solver
to produce the output coefficients seen in Table~\ref{tab:model}. Our method to
generate the training data is designed carefully to ensure that the
least-squares solver can estimate meaningful, positive coefficients. 

To generate model training data, we start
stressing as \textit{few} architectural units as possible, before subsequently
adding additional computational elements on top. For example, the idle
experiments with varied temperature and rail voltage (see Section~\ref{sec:subthreffects}) is
a good entry point, where the rails are only powered, processors idle, and
only temperature and voltage changes, having a substantial impact on only subthreshold leakage and a minimum
impact on dynamic power on each rail. Subsequently, the CPU cores can start
running more computationally intensive workloads, stressing various cache
hierarchies, RAM and instruction types, before the GPU rail is turned
on, in turn running CUDA kernels designed to stress the various dynamic
power predictors in that domain. The benchmarks used are similar to those used
for power modelling on the Tegra~K1~\cite{StokkePhD}, with some changes to and
additional CUDA kernels, due to changes in architecture and tweaking of the
training data. All benchmarks that actively stimulate the usage of dynamic power are run over \textit{all possible} memory and
processor frequencies, and over all possible core configurations, that is, four
possible CPU cores online. This helps vary clock frequencies and rail voltages,
and creates natural variation in the hardware access rates, aiding in the
estimation of dynamic power coefficients. 

Finally, when the resulting dataset is input to the non-linear least-squares
solver, an additional guess must be given. For the subthreshold leakage
coefficients $\omega$ and thermal voltage coefficient $\alpha$, we resorted to
use values close to those that can be estimated with the same solver, without
any initial guess, on the experiments in Section~\ref{sec:subthreffects}. These
are $\omega = 5.0$ and $\alpha = 2 * 10^{-3}$. All dynamic power coefficients
have an initial guess of $2.0 \frac{nC}{V}$, with the exception of $\rho_{gpu}$,
$\rho_{mem}$ and $\rho_{cpu}$, with initial guesses of $200.0 \frac{pC}{V}$,
$200.0 \frac{pC}{V}$ and $300.0 \frac{pC}{V}$. The base power initial guess is
$P_{base} = 3.0 W$. All resulting model coefficients are positive and can be seen in
Table~\ref{tab:model}. 

