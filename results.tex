
\section{Experimental Results}

In this section, we verify our power model for the \tegra by subjecting it to
rigorous testing through a wide range of idle and processing scenarios, with
varying temperature and operating (voltage and frequency) conditions. In this
section, we will extend our verification approach to not only study the
prediction error towards the total power usage of the \jetson, but also use the
built-in rail measurement sensors on the HP and GPU rails. We are the first to
investigate how a power model built entirely based on total power measurements,
performs with the individual power draw of the components such as the \tegra's
CPU and GPU. 

\subsection{Idle Platform Power Usage}

\begin{figure*}
    \begin{center}
        \begin{subfigure}[t]{0.45\textwidth}
            \includegraphics[scale=0.18, trim = 0cm 0cm 0cm 0cm, clip = true]{tempnano.pdf}
            \caption{\label{fig:temppownano}Tegra Nano.}
        \end{subfigure}
        \begin{subfigure}[t]{0.45\textwidth}
            \includegraphics[scale=0.18, trim = 0cm 0cm 0cm 0cm, clip = true]{tempx1.pdf}
            \caption{\label{fig:temppowx1}Tegra X1.}
        \end{subfigure}
    \end{center}
    \caption{\label{fig:temppow}Average SoC temperature, measured power and estimated power in idle tests.}
\end{figure*}

We start our evaluation of the power models for the Tegra Nano and X1 by studying the platforms without any active workloads running.
This is essentially the same as the primary \textit{idle tests} used for model training (see Section~\ref{sec:modtrain}) with no modifications. 
However, we redo these experiments as the \textit{model training data} should never be used to evaluate the model itself.
The experiments are run as follows.
The respective Tegra system is first allowed to rest until an average SoC temperature of 20-30~C is reached.
Then, the experiment is started and the Tegra system cycles through all available, processor and memory frequencies, as well all possible number of CPU cores. 
At each cycle, the Tegra system is allowed to sleep for 850~ms while periodically waking up to collect system information and utilisation statistics. 
Our measurement setup collects power and voltage readings from the SoC, and the test takes approximately 10 to 15 minutes. 
During the test, an external heater is physically connected to the heat sink to help increase the SoC temperature. 
The SoC temperature ends at approximately 70~C to 80~C towards the end of the test, which is close to the maxixmum operating conditions of the chip (85~C).

The measured power, estimated power and average SoC temperature is shown in Figure~\ref{fig:temppownano} for the Nano, and in Figure~\ref{fig:temppowx1} for the X1.
In terms of model accuracy, both models are underestimating power usage.
For example, the Tegra Nano model error rate is only -1.3~\% on average relative to the measured power. 
For the Tegra X1, the underestimation is larger at -5.8~\%. 
This number is large compared to our earlier work.
However, if we study Figure~\ref{fig:temppowx1}, we observe that from around sample 10000 the underestimation is visibly larger than before.
For example, if we split the dataset at sample 10000, the average error below sample 10000 is only -4.7~\%.
The average error above sample 10000 is -7.9~\%, indicating that the model is less accurate at higher temperature.
However, on average the error rate is low, and close to the model error rates of our previous models for the Tegra K1.
These also did not consider temperature and shows that our model is capable of estimating power usage to a very high degree across a wide range of operating temperatures, frequencies and core combinations.

\begin{figure*}
    \begin{center}
        \begin{subfigure}[t]{0.45\textwidth}
            \includegraphics[scale=0.18, trim = 0cm 0cm 0cm 0cm, clip = true]{idlerailnano.pdf}
            \caption{\label{fig:powrailnano}Tegra Nano.}
        \end{subfigure}
        \begin{subfigure}[t]{0.45\textwidth}
            \includegraphics[scale=0.18, trim = 0cm 0cm 0cm 0cm, clip = true]{idlerailx1.pdf}
            \caption{\label{fig:powrailx1}Tegra X1.}
        \end{subfigure}
    \end{center}
    \caption{\label{fig:idlerail}Measured and estimated GPU and CPU rail power.}
\end{figure*}

A unique feature of the Tegra development kits that success the Tegra~K1 is the ability to measure the power usage of individual power rails.
The power usage of embedded hardware units is measured using the INA3221 rail monitor (see Section~\ref{sec:tegrahw}), which is capable of measuring both the CPU and GPU rail, and the total power usage of the SoC.
Contrary to verifying the model with only an external (but highly accurate) measurement, this gives us a unique opportunity to test the accuracy of the model with sensors that are nested much deeper into the hardware architecture.
While we have shown in previous work that our power modelling methodology is highly accurate, how accurately does it predict power usage of individual units?
In Figure~\ref{fig:idlerail}, we plot the measured and estimated power usage of the Tegra systems' CPU and GPU rail, where Figure~\ref{fig:powrailnano} and \ref{fig:powrailx1} is for the Nano and X1, respectively.
CPU and GPU rail power is given by:

\begin{equation}
    P_{rail} = P_{leak} + P_{ccl} + \sum^{i \in \mathbb{I} }P_{i}
    \label{equ:railpower}
\end{equation}

In Equation~\ref{equ:railpower}, $P_{leak}$ is the temperature-dependent leakage component, $P_{ccl}$ is the cycle power, $\mathbb{I}$ is the set of all types of instructions on that rail, and $P_{i}$ is the contribution to power from instruction $i$.
For the CPU, there is only one single instruction counter, whereas for the GPU, there are eleven (see Section~\ref{dummy}).

The measured and estimated CPU and GPU rails of the Tegra Nano and X1, respectively, is shown in Figure~\ref{fig:powrailnano} and \ref{fig:powrailx1}.
Note that Figure~\ref{fig:idlerail} follows the same discrete timeline as in Figure~\ref{fig:temppow}, following our previous discussion.
In general, the modelled power follows the same trends as the measurement. 
We can see that the CPU rail estimations mispredict power most. 
For example, for the Tegra Nano, the average CPU model error is -27.4~\%.
The GPU model error is only 6.5~\%.
For the Tegra X1, CPU model error is 33.7~\% and GPU model error is only 8.6~\%.
A CPU model error of around 30~\% is very high compared to the relatively low model error seen with external power measurement of -1.3~\% and -5.8~\%.
However, note that this is an extreme case:
In idle mode, the CPU is almost always sleeping, and the signal (power measurement) is consequently very low. 
During this time the CPU is clock gating its core.
The CPU is waking up in very short intervals, enabling its clock to execute instructions. 
This may cause high dynamics in the signal that are difficult to capture for the INA3221 sensor, which uses a single ADC to measure rail voltage and current in a time-multiplexted manner.
Conversely, we have not found any evidence to suggest that the GPU is clock gating its core.
Therefore the current consumption on the GPU rail will be more stable over time. 
Additionally, a possible reason for \textit{overestimation} may be that the rail measurement sensors are mounted \textit{after} various power regulators on the chip.
In this case, the overall power model is capturing conversion loss beyond that measured using the rail sensors.
Of course these results may also suggest that the CPU model needs further development, or some combination of all the above. 

\begin{figure*}
    \begin{center}
        \begin{subfigure}[t]{0.28\textwidth}
            \includegraphics[scale=0.1, trim = 0cm 0cm 0cm 0cm, clip = true]{gpuleak.pdf}
            \caption{\label{fig:gpuleak}GPU rail.}
        \end{subfigure}
        \begin{subfigure}[t]{0.28\textwidth}
            \includegraphics[scale=0.1, trim = 0cm 0cm 0cm 0cm, clip = true]{cpuleak.pdf}
            \caption{\label{fig:cpuleak}CPU rail.}
        \end{subfigure}
        \begin{subfigure}[t]{0.28\textwidth}
            \includegraphics[scale=0.1, trim = 0cm 0cm 0cm 0cm, clip = true]{otherleak.pdf}
            \caption{\label{fig:otherleak}Other.}
        \end{subfigure}

    \end{center}
    \caption{\label{fig:railleak}Estimated leakage.}
\end{figure*}


\subsection{Comparison of Video Encoders}

\subsection{Cooling and Design Power}

