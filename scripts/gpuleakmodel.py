#! /usr/bin/python


# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
from model_x1 import *
import pandas as pd
import numpy  as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d, Axes3D
from numpy import linspace, meshgrid
from matplotlib.mlab import griddata
import matplotlib
from matplotlib import cm
#import scipy.optimize as optimize
#from scipy.optimize import fsolve, root
from scipy.optimize import curve_fit
from common_x1 import *

font = {'size'   : 20} # 30

matplotlib.rc('font', **font)

# GPU idle
#f = pd.read_csv('/home/krisrst/test_results/leak_idle/1/all.csv')
# Core idle
#f = pd.read_csv('/home/krisrst/test_results/leak_idle/2/all_nogpuv.csv')
# HP idle
#f = pd.read_csv('/home/krisrst/test_results/leak_idle/3/all_cpu0.csv')
#f = pd.read_csv('/home/krisrst/test_results/leak_idle/3/all_cpu1.csv')
#f = pd.read_csv('/home/krisrst/test_results/leak_idle/3/all_cpu2.csv')
#f = pd.read_csv('/home/krisrst/test_results/leak_idle/3/all_cpu3.csv')

# C63
f = pd.read_csv('/home/krisrst/test_results/workloads/c63/old-power-code/all.csv')

# Idle CPU inst
cpu_inst = add_data( f )

cpuinst_p = [sum(x) for x in zip(g_cpuinst[0])]

g_zeros = np.array(cpuinst_p) * 0.0

input_data      = [ g_temp_avg, g_gpuv, g_corev, g_hpv, 
                   g_cpufrq_p, g_gpufrq_p, g_memfrq_p,
                   g_com_l1l2_p, 
                   g_com_l2ram_p,
                   g_mem_actmon_p, 
                   
                   g_zeros, g_zeros,

                   cpuinst_p,
                   
                   #g_cpuinst[3], 
                   g_zeros, g_zeros,
                   g_zeros, g_zeros, g_zeros,
                   
                   g_gpu_ccl,                       
                   g_gpu_int,
                   g_gpu_f32,
                   g_gpu_f64,
                   g_gpu_misc,
                   g_gpu_ctrl,
                   g_gpu_cnv,
                   g_gpu_l2r,
                   g_gpu_l2w,
                   g_gpu_l1,
                   g_gpu_ldst,
                   g_cores,
                   g_gpu_f16,
                   g_gpu_shr,
                   g_gpu_shw,
                   g_gpu_act
                   ]

def getrow( arr, i):
    return [row[i] for row in arr]

result = []
xs = np.linspace(0, len(g_power)-1, num=len(g_power))
for i in xs:
    row = getrow( input_data, int(i))
    result.append( func_hppm(row, p_kw_gpu, p_t_c, p_base_power, 
                   p_kw_core,
                   p_kw_percpu,
                   p_cpu_cycles, 
                   p_gpu_cycles, 
                   p_mem_cycles,
                   p_com_l1l2, 
                   0.0,
                   p_actmon_act,
                   0.0, 0.0,
                   p_active_cpuinst,
                   0.0, 0.0,
                   0.0, 0.0, 0.0,
                   p_gpu_ccl,
                   p_gpu_int,
                   p_gpu_f32,
                   p_gpu_f64,
                   p_gpu_misc,
                   p_gpu_ctrl,
                   p_gpu_cnv,
                   p_gpu_l2r,
                   0.0, #p_gpu_l2w,
                   p_gpu_l1,
                   0.0, #p_gpu_ldst,
                   p_gpu_f16,
                   0.0, #p_gpu_shr,
                   p_gpu_shw,
                   p_gpu_act) )


ind = np.linspace(0, len(g_power), num=len(g_power))


fig = plt.figure()
ax = fig.add_subplot(111)

plt.scatter(xs, result, color='r', label="Estimated Power")
plt.scatter(xs, g_power, color='c', label="Measured Power")
#plt.scatter(g_temp_avg, g_estgpuccl, color='b', label="Estimated GPU Cycle Power")
#plt.scatter(g_temp_avg, g_gpupow, label="Measured GPU Rail Power")

#plt.scatter(g_temp_avg, g_estcpupow, color='r', label="Estimated CPU Rail Power")
#plt.scatter(g_temp_avg, g_estcpuleak, color='c', label="Estimated CPU Leak Power")
#plt.scatter(g_temp_avg, g_hppow, label="Measured CPU Rail Power")

#plt.scatter(g_temp_avg, (g_estcoreleak) , color='r', label="Estimated Core Leak Power")
#plt.scatter(g_temp_avg, [p[0]-p[1] for p in zip(g_corepow, g_basepow)], color='c', label="Measured Core Power")

#plt.scatter(g_temp_avg, g_estpow, color='r', label="Estimated Platform Power")
#plt.scatter(g_temp_avg, g_power, color='c', label="Measured Platform Power")

#plt.legend()
#plt.xlabel("Sample")
#plt.ylabel("Power [W]")

lpad = 28.0
elev = 30
azim = -130

if False:
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    
    ax.scatter(g_temp_avg, g_corev, g_power, color='c', label="Measured Core Power")
    ax.scatter(g_temp_avg, g_corev, g_estpow, color='g', label="Estimated Core (Leak) Power")
    
    ax.set_xlabel("Average SoC Temperature [T]", labelpad=lpad)
    ax.set_ylabel("Core Rail Voltage [V]", labelpad=lpad)
    ax.set_zlabel("Power [W]", labelpad=lpad)
    
    ax.view_init(elev=elev, azim=azim)
    
    plt.legend()






