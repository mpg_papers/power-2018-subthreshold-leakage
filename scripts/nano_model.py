5#! /usr/bin/python


# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
#from sklearn import linear_model
import pandas as pd
#import numpy  as np
#import matplotlib.pyplot as plt
#from mpl_toolkits.mplot3d import axes3d, Axes3D
#from numpy import linspace, meshgrid
#from matplotlib.mlab import griddata
import matplotlib
import common_nano as common
#import common_x1 as common
#import scipy.optimize as optimize
#from scipy.optimize import fsolve, root
#from scipy.optimize import curve_fit
from scipy.optimize import fmin
#from scipy.optimize import minimize
#import scipy.optimize as optimize

font = {'size'   : 10} # 30

matplotlib.rc('font', **font)

f = pd.read_csv('/home/krisrst/power_models/nano/sensor_all_temp.csv')
common.add_data( f )

f = pd.read_csv('/home/krisrst/power_models/nano/sensor_all_cpu.csv')
common.add_data( f )

# Extend cpu instruction array
common.extend_cpuinst()

data = common.get_model_input_data()

guess = common.get_model_guess()

def opt(x):
    err = common.func_hppm(data, x[0], x[1], x[2], x[3], x[4], x[5], x[6], 
                          x[7], x[8], x[9], x[10], x[11], x[12], x[13], x[14], 
                          x[15], x[16], x[17], x[18], x[19], x[20], x[21], x[22], 
                          x[23], x[24], x[25],
                          x[26], x[27], x[28], x[29], x[30], x[31], x[32])
    
    return err

#CPU-TEMP
xopt = fmin(func=opt, x0=guess, maxfun=10E8, xtol=1E-1, ftol=1E-1)#, xtol=1E-1, ftol=1E-1

[xopt, err] = common.get_last_positive()

# Clear worthless GPU estimates
xopt[19:33] = 0

names = common.get_mode_input_data_stringified()

print "CPU predictors"
for e in zip(names, xopt):
    print "%s : %s" % (e[0], e[1])


#######
# GPU #
# GPU #
# GPU #
#######
common.clear_globals()

f = pd.read_csv('/home/krisrst/power_models/nano/sensor_all_gpu.csv')
common.add_data( f )

common.extend_cpuinst()

data = common.get_model_input_data_single()
[est_pow, est_cpu, est_gpu] = common.common_estimate_power(data, xopt)

common.g_power = common.g_power - est_pow

data2 = common.get_model_input_data()

guess = common.get_model_guess()

def gpu_opt(x):
    err = common.func_hppm_gpu(data2, x[0], x[1], x[2], x[3], x[4], x[5], x[6], 
                          x[7], x[8], x[9], x[10], x[11], x[12], x[13], x[14], 
                          x[15], x[16], x[17], x[18], x[19], x[20], x[21], x[22], 
                          x[23], x[24], x[25],
                          x[26], x[27], x[28], x[29], x[30], x[31], x[32])
    
    return err

#GPU
guess = common.get_model_guess()
xoptg = fmin(func=gpu_opt, x0=guess, maxfun=10E8, xtol=1E-10, ftol=1E-10)#, xtol=1E-1, ftol=1E-1

[xoptg, err] = common.get_last_positive()


xopt[19:33] = xoptg[19:33]

names = common.get_mode_input_data_stringified()

for e in zip(names, xopt):
    print "%s : %s" % (e[0], e[1])












