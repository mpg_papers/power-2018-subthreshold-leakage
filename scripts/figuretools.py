#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat May 18 18:13:07 2019

@author: krisrst
"""

import matplotlib
import matplotlib.pyplot as plt

matplotlib.rcParams.update({'font.size': 32})

c_temp    = '#ce42f4'
c_power   = 'b'
c_estpow  = '#f9c44f'
c_power2  = '#23137f'
c_estpow2 = 'c'

def leakage_plot(t1, l1, n1, t2, l2, n2, rail):
    
    plt.figure()
    
    ax1 = plt.gca()
    
    ax1.scatter(t1, l1, label=n1)
    ax1.scatter(t2, l2, label=n2)
    
    ax1.set_ylabel("Leakage Power [W]")
    ax1.set_xlabel("Temperature [C]")
    ax1.set_title("%s Rail" % (rail))
    
    ax1.legend(loc="upper left")

def temp_power_plot(temp, power, estpow, title):
    
    plt.figure()
    
    plt.title(title)
    
    ax1 = plt.gca();
    
    ax1.plot(temp, color=c_temp, label="Average SoC Temperature")
    ax1.set_ylabel("Temperature [C]")
    ax1.set_xlabel("Sample")
    ax1.legend(loc="upper left")
    
    ax2 = ax1.twinx()    
    ax2.plot(power, color=c_power, label="Power (Measured)")
    ax2.plot(estpow, color=c_estpow, label="Power (Estimated)")
    ax2.set_ylabel("Power [W]")
    ax2.legend(loc="lower right")
    
def power_plot(power, estpow, title):
    
    plt.figure()
    
    plt.title(title)
    
    ax1 = plt.gca();
    
    ax1.plot(power, color=c_power, label="Power (Measured)")
    ax1.plot(estpow, color=c_estpow, label="Power (Estimated)")
    ax1.set_ylabel("Power [W]")
    ax1.legend(loc="lower right")
    
    
def power_plot_dual(p1, e1, l1, p2, e2, l2):
    
    plt.figure()
    
    ax1 = plt.gca();
    
    ax1.plot(p1, color=c_power, label="%s Power (Measured)" % (l1))
    ax1.plot(e1, color=c_estpow, label="%s Power (Estimated)" % (l1))
    
    ax1.plot(p2, color=c_power2, label="%s Power (Measured)" % (l2))
    ax1.plot(e2, color=c_estpow2, label="%s Power (Estimated)" % (l2))
    
    ax1.set_ylabel("Power [W]")
    ax1.legend(loc="upper left")
    ax1.set_xlabel("Sample")
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    