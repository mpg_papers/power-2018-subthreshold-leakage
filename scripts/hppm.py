# -*- coding: utf-8 -*-
"""
Created on Sun Sep 23 11:57:32 2018

@author: krisrst
"""

"""
High-Precision Power Modelling python module for the Nano and X1.
"""

import numpy as np
import matplotlib.pyplot as plt

temp_t20 = 25.0E-3
temp_coeff = 3.0E-3

v_thresh_cpu = 0.8
v_thresh_gpu = 0.8
v_thresh_core = 0.8

def func_temp( temp, coeff ):
    
    _d = ((np.array(temp) * coeff) + temp_t20)
    return _d

g_zeros = []
g_temp_avg = []
g_temp_mem = []
g_temp_pll = []
g_temp_gpu = []
g_temp_cpu = []
g_hpv = []
g_corev = []
g_gpuv = []
g_memv = []
g_cores = []
g_cpufrq_p = []
g_gpufrq_p = []
g_memfrq_p = []
g_com_l1l2_p = []
g_com_l2ram_p = []
g_mem_actmon_p = []
g_power = []
g_cpuinst_idle_cpu = []
g_cpuinst_idle_gpu = []
g_cpuinst_int = []
g_cpuinst_idle_core = []
g_cpu_l1drf_tot = []
g_cpu_l1irf_tot = []
g_cpu_l2drf_tot = []
g_cpu_l2dwb_tot = []
g_cpu_pow= []

g_gpu_int = []
g_gpu_f16 = []
g_gpu_f32 = []
g_gpu_f64 = []
g_gpu_misc = []
g_gpu_ctrl = []
g_gpu_cnv = []
g_gpu_l2r = []
g_gpu_l2w = []
g_gpu_l1 = []
g_gpu_ldst = []
g_gpu_ccl = []
g_gpu_shr = []
g_gpu_shw = []
g_gpu_pow = []
g_gpu_act = []

g_cpuinst = []
g_cpuinst_any = []

mod_otherleak = []
mod_gpuleak = []
mod_gpufreq = []

mod_gpu_int = []
mod_gpu_f16 = []
mod_gpu_f32 = []
mod_gpu_f64 = []
mod_gpu_misc = []
mod_gpu_ctrl = []
mod_gpu_cnv = []
mod_gpu_l2r = []
mod_gpu_l2w = []
mod_gpu_l1 = []
mod_gpu_ldst = []
mod_gpu_ccl = []
mod_gpu_shr = []
mod_gpu_shw = []

mod_cpu_inst = []
mod_cpu_ccl = []
mod_cpu_leak = []

g_depth = 0

def dictionarise_xopt(xopt):
    
    dopt = {}
    
    dopt['kw_gpu'] = xopt[0]
    dopt['alpha'] = xopt[1]
    dopt['base'] = xopt[2]
    dopt['kw_oth'] = xopt[3]
    dopt['kw_cpu'] = xopt[4]
    dopt['ccl_cpu'] = xopt[5]
    dopt['ccl_gpu'] = xopt[6]
    dopt['ccl_mem'] = xopt[7]
    dopt['l1l2'] = xopt[8]
    dopt['l2ram'] = xopt[9]
    dopt['actmon'] = xopt[10]
    dopt['inst0'] = xopt[11]
    dopt['inst1'] = xopt[12]
    dopt['inst2'] = xopt[13]
    dopt['inst3'] = xopt[14]
    dopt['inst4'] = xopt[15]
    dopt['inst5'] = xopt[16]
    dopt['inst6'] = xopt[17]
    dopt['inst7'] = xopt[18]
    dopt['gpu_ccl'] = xopt[19]
    dopt['gpu_int'] = xopt[20]
    dopt['gpu_f32'] = xopt[21]
    dopt['gpu_f64'] = xopt[22]
    dopt['gpu_misc'] = xopt[23]
    dopt['gpu_ctrl'] = xopt[24]
    dopt['gpu_cnv'] = xopt[25]
    dopt['gpu_l2r'] = xopt[26]
    dopt['gpu_l2w'] = xopt[27]
    dopt['gpu_l1'] = xopt[28]
    dopt['gpu_ldst'] = xopt[29]
    dopt['gpu_f16'] = xopt[30]
    dopt['gpu_shr'] = xopt[31]
    dopt['gpu_shw'] = xopt[32]
    #dopt[''] = xopt[33]
    
    return dopt

def workload_energy( workloads ):
    
    for w in workloads:
        _s = 0
        
        for k in w:
            for key in k.keys():
                try:
                    _s = _s + k[key]['power']
                except TypeError:
                    continue
                
        print("Total power : %s" % (_s))

def gpu_kernel_hpcs( fname, dopt ):
    f = open(fname)
    
    # We will return a list of dictionaries containing the predictors
    kernel_list = []
    
    while True:
        
        # Create an empty dictionary for this kernel
        kernel_dict = {}
        
        # Header of type <struct kpc (...)>
        header = f.readline()
        
        if header == '':
            break
        
        if not 'struct kpc' in header:
            print("Invalid header format (%s)" % (header))
            break
        
        # Next is the SHA
        sha = int(f.readline().split(',')[0])
        
        print("Create kernel dictionary for SHA %s" % (sha))
        kernel_dict["SHA"] = sha
        
        # Parse in the rest of the kernel
        while True:
            line = f.readline()
            
            # End of structure representation
            if 'INVALID' in line:
                f.readline() # };
                # Next is the kernel name
                
                line = f.readline()
                
                name = line.split('(')[1].split(')')[0]
                
                kernel_dict['function'] = name
                
                break
            
            # Just split on comma and add key
            tup = line.split(',')
            
            # 0 = name, 1 = hardware register, 2 = value
            kernel_dict[tup[0]] = {}
            
            kernel_dict[tup[0]]['power'] = 0
            kernel_dict[tup[0]]['count'] = int(tup[2])
            
        # Compute the power components..
        # The resulting unit here is [C per V]; or [Ampere-seconds per V]
        kernel_dict['ELAPSED_CYCLES_SM']['power'] = kernel_dict['ELAPSED_CYCLES_SM']['count'] * dopt['gpu_ccl']
        kernel_dict['INST_BIT_CONVERT']['power'] = kernel_dict['INST_BIT_CONVERT']['count'] * dopt['gpu_cnv']
        kernel_dict['INST_CONTROL']['power'] = kernel_dict['INST_CONTROL']['count'] * dopt['gpu_ctrl']
        kernel_dict['INST_FP_16']['power'] = kernel_dict['INST_FP_16']['count'] * dopt['gpu_f16']
        kernel_dict['INST_FP_32']['power'] = kernel_dict['INST_FP_32']['count'] * dopt['gpu_f32']
        kernel_dict['INST_FP_64']['power'] = kernel_dict['INST_FP_64']['count'] * dopt['gpu_f64']
        kernel_dict['INST_INTEGER']['power'] = kernel_dict['INST_INTEGER']['count'] * dopt['gpu_int']
        kernel_dict['INST_MISC']['power'] = kernel_dict['INST_MISC']['count'] * dopt['gpu_misc']
        
        kernel_dict['L2R'] = {}
        kernel_dict['L2R']['count'] = kernel_dict['L2_SUBP0_READ_SECTOR_QUERIES']['count'] + kernel_dict['L2_SUBP1_READ_SECTOR_QUERIES']['count'] - (kernel_dict['L2_SUBP0_READ_SECTOR_MISSES']['count'] + kernel_dict['L2_SUBP1_READ_SECTOR_MISSES']['count'])
        
        kernel_dict['L2W'] = {}
        kernel_dict['L2W']['count'] = kernel_dict['L2_SUBP0_WRITE_SECTOR_QUERIES']['count'] + kernel_dict['L2_SUBP1_WRITE_SECTOR_QUERIES']['count'] - (kernel_dict['L2_SUBP0_WRITE_SECTOR_MISSES']['count'] + kernel_dict['L2_SUBP1_WRITE_SECTOR_MISSES']['count'])
        
        kernel_dict['L1R'] = {}
        kernel_dict['L1R']['count'] = kernel_dict['L1_TEX0_CACHE_SECTOR_QUERIES']['count']  + kernel_dict['L1_TEX1_CACHE_SECTOR_QUERIES']['count'] - (kernel_dict['L1_TEX0_CACHE_SECTOR_MISSES']['count']  + kernel_dict['L1_TEX1_CACHE_SECTOR_MISSES']['count'])
        
        kernel_dict['L1W'] = {}
        kernel_dict['L1W']['count'] = kernel_dict['SHARED_STORE_TRANSACTIONS']['count']

        
        kernel_dict['L1W']['power'] = kernel_dict['L1W']['count'] * dopt['gpu_shw']
        kernel_dict['L1R']['power'] = kernel_dict['L1R']['count'] * dopt['gpu_l1']
        kernel_dict['L2W']['power'] = kernel_dict['L2W']['count'] * dopt['gpu_l2w']
        kernel_dict['L2R']['power'] = kernel_dict['L2R']['count'] * dopt['gpu_l2r']
        
        # Delete unwanted keys
        del kernel_dict['L2_SUBP0_READ_SECTOR_QUERIES']
        del kernel_dict['L2_SUBP1_READ_SECTOR_QUERIES']
        del kernel_dict['L2_SUBP0_WRITE_SECTOR_QUERIES']
        del kernel_dict['L2_SUBP1_WRITE_SECTOR_QUERIES']
        del kernel_dict['L2_SUBP0_READ_SECTOR_MISSES']
        del kernel_dict['L2_SUBP1_READ_SECTOR_MISSES']
        del kernel_dict['L2_SUBP0_WRITE_SECTOR_MISSES']
        del kernel_dict['L2_SUBP1_WRITE_SECTOR_MISSES']
        del kernel_dict['SHARED_STORE_TRANSACTIONS']
        del kernel_dict['SHARED_LOAD_TRANSACTIONS']
        del kernel_dict['L1_TEX0_CACHE_SECTOR_MISSES']
        del kernel_dict['L1_TEX1_CACHE_SECTOR_MISSES']
        del kernel_dict['L1_TEX0_CACHE_SECTOR_QUERIES']
        del kernel_dict['L1_TEX1_CACHE_SECTOR_QUERIES']
        del kernel_dict['INST_LD_ST']
        
        # Add the kernel to the output kernel list
        kernel_list.append(kernel_dict)
        
    f.close()
    
    return kernel_list
        

def add_data( f ):
    
    global g_temp_avg
    global g_temp_mem
    global g_temp_pll
    global g_temp_gpu
    global g_temp_cpu
    
    
    global g_hpv
    global g_corev
    global g_gpuv
    global g_memv
    global g_cores
    global g_cpufrq_p
    global g_gpufrq_p
    global g_memfrq_p
    global g_com_l1l2_p
    global g_com_l2ram_p
    global g_mem_actmon_p
    global g_cpuinst
    global g_depth
    global g_cpu_l1drf_tot
    global g_cpu_l1irf_tot
    global g_cpu_l2drf_tot
    global g_cpu_l2dwb_tot
    global g_gpu_ccl
    global g_gpu_int
    global g_gpu_f16
    global g_gpu_f32
    global g_gpu_f64
    global g_gpu_misc
    global g_gpu_ctrl
    global g_gpu_cnv
    global g_gpu_l2r
    global g_gpu_l2w
    global g_gpu_l1
    global g_gpu_ldst
    global g_gpu_shr
    global g_gpu_shw
    global g_gpu_act
    global g_zeros
    global g_gpu_pow
    global g_cpu_pow

    ones = np.ones(len(f['Timestamp']))
    xs   = range(len(f['Timestamp']))
    dur = f['Duration'] / 1E3
    
    """
    Increase array size for cpuinsts
    """
    for e in g_cpuinst:
        e.extend( 0 * np.ones( len(ones) ))
    
    power = f['POWER']
    g_power.extend( power )
    
    """
    Platform voltages.
    """
 
    """
    hpv = f['VDD2']
    gpuv = f['VDD1']
    """
    
    hpv = f['VDD_CPU']
    gpuv = f['VDD_GPU']
    
    gpup = f['POW_GPU']
    cpup = f['POW_CPU']
    
    g_gpu_pow.extend(gpup)
    g_cpu_pow.extend(cpup)
    
    #corev = f['VDD_CORE']
    memv = ones * 1.1 # 1.1 V LPDDR4
    
    g_hpv.extend( hpv )
    #g_corev.extend( corev )
    g_gpuv.extend( gpuv )
    g_memv.extend( memv )
    
    """
    Square platform voltages.
    """
    hpv2 = hpv * hpv
    gpuv2 = gpuv * gpuv
    #corev2 = corev * corev
    memv2 = memv * memv
    
    """
    Leakage and Temperature
    """
    
    temp_mem = f['TEMP_MEM']
    temp_pll = f['TEMP_PLL']
    temp_gpu = f['TEMP_GPU']
    temp_cpu = f['TEMP_CPU']
    
    g_temp_mem.extend(temp_mem)
    g_temp_pll.extend(temp_pll)
    g_temp_gpu.extend(temp_gpu)
    g_temp_cpu.extend(temp_cpu)
    
    temp_avg = (temp_mem + temp_pll + temp_gpu + temp_cpu) / 4
    g_temp_avg.extend( temp_avg )
    
    core0 = ones
    core1 = f['CORE1']
    core2 = f['CORE2']
    core3 = f['CORE3']
    
    core_tot = core0 + core1 + core2 + core3
    g_cores.extend(core_tot)    
    
    """
    GPU
    """
    gpu_ccl = f['ELAPSED_CYCLES_SM']
    gpu_int = f['INST_INTEGER']
    gpu_f16 = f['INST_FP_16']
    gpu_f32 = f['INST_FP_32']
    gpu_f64 = f['INST_FP_64']
    gpu_ctrl = f['INST_CONTROL']
    gpu_misc = f['INST_MISC']
    gpu_cnv = f['INST_BIT_CONVERT']
    gpu_ldst = f['INST_LD_ST']
    gpu_shr  = f['SHARED_LOAD_TRANSACTIONS']
    gpu_shw  = f['SHARED_STORE_TRANSACTIONS']
    
    gpu_l2r = f['L2_SUBP0_READ_SECTOR_QUERIES']  + f['L2_SUBP1_READ_SECTOR_QUERIES']
    gpu_l2r -= (f['L2_SUBP0_READ_SECTOR_MISSES']  + f['L2_SUBP1_READ_SECTOR_MISSES'])
        
    gpu_l2w = f['L2_SUBP0_WRITE_SECTOR_QUERIES'] + f['L2_SUBP1_WRITE_SECTOR_QUERIES']
    gpu_l2w -= (f['L2_SUBP0_WRITE_SECTOR_MISSES'] + f['L2_SUBP1_WRITE_SECTOR_MISSES'])
    
    gpu_l1r = f['L1_TEX0_CACHE_SECTOR_QUERIES'] + f['L1_TEX1_CACHE_SECTOR_QUERIES']
    gpu_l1r -= f['L1_TEX0_CACHE_SECTOR_MISSES'] + f['L1_TEX1_CACHE_SECTOR_MISSES']
    
    gpu_ccl_p  = gpu_ccl  * gpuv2 / dur
    gpu_int_p  = gpu_int  * gpuv2 / dur
    gpu_f16_p  = gpu_f16  * gpuv2 / dur
    gpu_f32_p  = gpu_f32  * gpuv2 / dur
    gpu_f64_p  = gpu_f64  * gpuv2 / dur
    gpu_ctrl_p = gpu_ctrl * gpuv2 / dur
    gpu_misc_p = gpu_misc * gpuv2 / dur
    gpu_cnv_p  = gpu_cnv  * gpuv2 / dur
    gpu_ldst_p = gpu_ldst * gpuv2 / dur
    gpu_l2r_p  = gpu_l2r  * gpuv2 / dur
    gpu_l2w_p  = gpu_l2w  * gpuv2 / dur
    gpu_l1_p   = gpu_l1r  * gpuv2 / dur
    gpu_shr_p  = gpu_shr  * gpuv2 / dur
    gpu_shw_p  = gpu_shw  * gpuv2 / dur
    
    g_gpu_ccl.extend(gpu_ccl_p)
    g_gpu_int.extend(gpu_int_p)
    g_gpu_f16.extend(gpu_f16_p)
    g_gpu_f32.extend(gpu_f32_p)
    g_gpu_f64.extend(gpu_f64_p)
    g_gpu_misc.extend(gpu_misc_p)
    g_gpu_ctrl.extend(gpu_ctrl_p)
    g_gpu_cnv.extend(gpu_cnv_p)
    g_gpu_l2r.extend(gpu_l2r_p)
    g_gpu_l2w.extend(gpu_l2w_p)
    g_gpu_l1.extend(gpu_l1_p)
    g_gpu_ldst.extend(gpu_ldst_p)
    g_gpu_shr.extend(gpu_shr_p)
    g_gpu_shw.extend(gpu_shw_p)
    
    """
    Misc.
    """
    cpu_cycles_tot = f['CPU0_CYCLES'] + f['CPU1_CYCLES'] + f['CPU2_CYCLES'] + f['CPU3_CYCLES']
    cpu_inst_tot = f['CPU0_INST_EXEC_SW'] + f['CPU1_INST_EXEC_SW'] + f['CPU2_INST_EXEC_SW']+ f['CPU3_INST_EXEC_SW']
    #cpu_l1drf_tot = f['CPU0_L1D_REFILL'] + f['CPU1_L1D_REFILL'] + f['CPU2_L1D_REFILL'] + f['CPU3_L1D_REFILL']
    #cpu_l1irf_tot = f['CPU0_L1I_REFILL'] + f['CPU1_L1I_REFILL'] + f['CPU2_L1I_REFILL'] + f['CPU3_L1I_REFILL']
    #cpu_l2drf_tot = f['CPU0_L2D_REFILL'] + f['CPU1_L2D_REFILL'] + f['CPU2_L2D_REFILL'] + f['CPU3_L2D_REFILL']
    #cpu_l2dwb_tot = f['CPU0_L2D_CACHE_WB'] + f['CPU1_L2D_CACHE_WB'] + f['CPU2_L2D_CACHE_WB'] + f['CPU3_L2D_CACHE_WB']
    
    #g_cpu_l1drf_tot.extend( (cpu_l1drf_tot - cpu_l2drf_tot + cpu_l1irf_tot) * hpv2 )
    #g_cpu_l1irf_tot.extend( cpu_l1irf_tot * hpv2 )
    #g_cpu_l2drf_tot.extend( cpu_l2drf_tot * hpv2 )
    #g_cpu_l2dwb_tot.extend( cpu_l2dwb_tot * hpv2 )
    
    #com_l1l2 = cpu_l1drf_tot + cpu_l1irf_tot
    #com_l2ram = cpu_l2drf_tot# + cpu_l2dwb_tot

    #com_l1l2_p = com_l1l2 * hpv2
    #com_l2ram_p = com_l2ram * hpv2
    #g_com_l1l2_p.extend( com_l1l2_p / dur )
    #g_com_l2ram_p.extend( com_l2ram_p / dur )
    
    cpu_inst_p = cpu_inst_tot * hpv2  / dur
    
    new_cpuinst = []
    new_cpuinst.extend(0 * np.ones( g_depth ))
    new_cpuinst.extend( cpu_inst_p )
    
    g_cpuinst_any.extend( cpu_inst_p )
    g_cpuinst.append( new_cpuinst )
    g_depth += len(ones)
    
    mem_actmon_p = f['ACTMON_ALL'] * memv2
    g_mem_actmon_p.extend( mem_actmon_p / dur )
    
    """
    Clock cycle predictors
    """
    gpufrq_p = f['GPUFREQ'] * gpuv2 * 1E3
    cpufrq_p = cpu_cycles_tot * hpv2
    memfrq_p = f['MEMFREQ'] * memv2 * 1E3
    
    g_cpufrq_p.extend(cpufrq_p / dur)
    g_gpufrq_p.extend(gpufrq_p)
    g_memfrq_p.extend(memfrq_p)
    
    """
    Special purpose
    """
    g_gpu_act.extend( (gpu_ccl / (f['GPUFREQ'] * 1000)) / dur )
    
    """
    Return workload-specific
    """
    return cpu_inst_p

def get_mode_input_data_stringified():
    return ( "KW_gpu",
        "t_c",
        "base", 
        "KW_core",
        "KW_cpu",
        "g_cpufrq_p", 
        "g_gpufrq_p", 
        "g_memfrq_p",
        "g_com_l1l2_p", 
        "g_com_l2ram_p",
        "g_mem_actmon_p",                    
        "g_cpuinst0",
        "g_cpuinst1",
        "g_cpuinst2",
        "g_cpuinst3",
        "g_cpuinst4",
        "g_cpuinst5",
        "g_cpuinst6",
        "g_cpuinst7",
        "g_gpu_ccl",                       
        "g_gpu_int",
        "g_gpu_f32",
        "g_gpu_f64",
        "g_gpu_misc",
        "g_gpu_ctrl",
        "g_gpu_cnv",
        "g_gpu_l2r",
        "g_gpu_l2w",
        "g_gpu_l1",
        "g_gpu_ldst",
        "g_gpu_f16",
        "g_gpu_shr",
        "g_gpu_shw")
        
def merge_cpuinsts():
    ones = len(g_power)
    all_inst = np.ones(ones) * 0
    for e in g_cpuinst:
        all_inst = all_inst + e
    
    g_cpuinst[0] = all_inst

def clear_globals():
    global g_temp_avg
    global g_hpv
    global g_corev
    global g_gpuv
    global g_memv
    global g_cores
    global g_cpufrq_p
    global g_gpufrq_p
    global g_memfrq_p
    global g_com_l1l2_p
    global g_com_l2ram_p
    global g_mem_actmon_p
    global g_cpuinst
    global g_depth
    global g_cpu_l1drf_tot
    global g_cpu_l1irf_tot
    global g_cpu_l2drf_tot
    global g_cpu_l2dwb_tot
    global g_gpu_ccl
    global g_gpu_int
    global g_gpu_f16
    global g_gpu_f32
    global g_gpu_f64
    global g_gpu_misc
    global g_gpu_ctrl
    global g_gpu_cnv
    global g_gpu_l2r
    global g_gpu_l2w
    global g_gpu_l1
    global g_gpu_ldst
    global g_gpu_shr
    global g_gpu_shw
    global g_gpu_act
    global g_zeros
    
    global mod_gpuleak
    global mod_gpufreq
    
    global mod_gpu_int
    global mod_gpu_f16
    global mod_gpu_f32
    global mod_gpu_f64
    global mod_gpu_misc
    global mod_gpu_ctrl
    global mod_gpu_cnv
    global mod_gpu_l2r
    global mod_gpu_l2w
    global mod_gpu_l1
    global mod_gpu_ldst
    global mod_gpu_ccl
    global mod_gpu_shr
    global mod_gpu_shw
    
    global mod_cpu_inst
    global mod_cpu_ccl
    global mod_cpu_leak
    
    global g_power
    
    g_zeros = []
    g_temp_avg = []
    g_hpv = []
    g_corev = []
    g_gpuv = []
    g_memv = []
    g_cores = []
    g_cpufrq_p = []
    g_gpufrq_p = []
    g_memfrq_p = []
    g_com_l1l2_p = []
    g_com_l2ram_p = []
    g_mem_actmon_p = []
    g_power = []
    g_cpu_l1drf_tot = []
    g_cpu_l1irf_tot = []
    g_cpu_l2drf_tot = []
    g_cpu_l2dwb_tot = []
    
    mod_cpu_inst = []
    mod_cpu_ccl = []
    mod_cpu_leak = []
    
    g_gpu_int = []
    g_gpu_f16 = []
    g_gpu_f32 = []
    g_gpu_f64 = []
    g_gpu_misc = []
    g_gpu_ctrl = []
    g_gpu_cnv = []
    g_gpu_l2r = []
    g_gpu_l2w = []
    g_gpu_l1 = []
    g_gpu_ldst = []
    g_gpu_ccl = []
    g_gpu_shr = []
    g_gpu_shw = []
    
    g_gpu_act = []
    
    g_cpuinst = []
    g_cpuinst_any = []
    g_depth = 0
    
    mod_otherleak = []
    mod_gpuleak = []
    mod_gpufreq = []
    
    mod_gpu_int = []
    mod_gpu_f16 = []
    mod_gpu_f32 = []
    mod_gpu_f64 = []
    mod_gpu_misc = []
    mod_gpu_ctrl = []
    mod_gpu_cnv = []
    mod_gpu_l2r = []
    mod_gpu_l2w = []
    mod_gpu_l1 = []
    mod_gpu_ldst = []
    mod_gpu_ccl = []
    mod_gpu_shr = []
    mod_gpu_shw = []
    
    
    
def extend_cpuinst():
    global g_cpuinst
    global g_depth
    
    rest = 8 - len(g_cpuinst)
    if rest < 0:
        print "ERROR! Too many cpuinst. The following will fail:"
    else:
        print "Extending %s times" % (rest)
    for e in range(rest):
        g_cpuinst.append(np.zeros(g_depth))
        
# Initial guess

guess_KW_gpu = 1
guess_KW_core = 1
guess_KW_cpu = 1
guess_t_c = 0.01
guess_base = 1
guess_cpu_load = 1e-9
guess_gpu_load = 1e-9
guess_mem_load = 1e-9
guess_l1l2 = 1E-9
guess_l2ram = 1E-9
guess_actmon = 1E-9
guess_inst = 1E-9
guess_nano = 1E-9
guess_pico = 1E-12
guess_gpu_act = 200E-3 # mW

"""
guess_KW_gpu = 1.0
guess_KW_core = 1.0
guess_KW_cpu = 1.0
guess_t_c = 1
guess_base = 1.0
guess_cpu_load = 1e-9
guess_gpu_load = 1e-9
guess_mem_load = 1e-9
guess_l1l2 = 1E-9
guess_l2ram = 1E-9
guess_actmon = 1E-9
guess_inst = 1E-9
guess_nano = 1E-6
guess_pico = 1E-12
guess_gpu_act = 200E-3 # mW
"""

def get_model_guess():
    return [guess_KW_gpu, guess_t_c, guess_base, 
                   guess_KW_core,
                   guess_KW_cpu,
                   guess_cpu_load, 
                   guess_gpu_load, 
                   guess_mem_load,
                   guess_l1l2, 
                   guess_l2ram, 
                   guess_actmon,
                   guess_inst,
                   guess_inst,
                   guess_inst,
                   guess_inst,
                   guess_inst,
                   guess_inst,
                   0.5,
                   0.5,
                   guess_nano,
                   guess_nano,
                   guess_nano,
                   guess_nano,
                   guess_nano,
                   guess_nano,
                   guess_nano,
                   guess_nano,
                   guess_nano,
                   guess_nano,
                   guess_nano,
                   guess_nano,
                   guess_nano,
                   guess_nano,
                   ]

def get_model_input_data_single():
    
    return ( g_temp_avg,
            g_gpuv,
            g_corev, 
            g_hpv, 
            g_cpufrq_p, 
            g_gpufrq_p, 
            g_memfrq_p,
            g_com_l1l2_p, 
            g_com_l2ram_p,
            g_mem_actmon_p,
            g_cpuinst[0],
            g_gpu_ccl,
            g_gpu_int,
            g_gpu_f32,
            g_gpu_f64,
            g_gpu_misc,
            g_gpu_ctrl,
            g_gpu_cnv,
            g_gpu_l2r,
            g_gpu_l2w,
            g_gpu_l1,
            g_gpu_ldst,
            g_cores,
            g_gpu_f16,
            g_gpu_shr,
            g_gpu_shw,
            g_power)

def get_model_input_data():
    
    return ( g_temp_avg,
            g_gpuv,
            g_corev, 
            g_hpv, 
            g_cpufrq_p, 
            g_gpufrq_p, 
            g_memfrq_p,
            g_com_l1l2_p, 
            g_com_l2ram_p,
            g_mem_actmon_p,
            g_cpuinst[0],
            g_cpuinst[1],
            g_cpuinst[2],
            g_cpuinst[3],
            g_cpuinst[4],
            g_cpuinst[5],
            g_cpuinst[6],
            g_cpuinst[7],
            g_gpu_ccl,                       
            g_gpu_int,
            g_gpu_f32,
            g_gpu_f64,
            g_gpu_misc,
            g_gpu_ctrl,
            g_gpu_cnv,
            g_gpu_l2r,
            g_gpu_l2w,
            g_gpu_l1,
            g_gpu_ldst,
            g_cores,
            g_gpu_f16,
            g_gpu_shr,
            g_gpu_shw,
            g_power)

calc_component_power = False

est_leak_core_pwr = []
est_leak_cpu_pwr = []
est_leak_gpu_pwr = []

def print_dbg_mod( str_, vals ):
    global calc_component_power
    if calc_component_power is False:
        return
    
    avg = np.average( vals )
    print "%s : %s W" % (str_, avg)
    
# The last model coefficients that produced a full set of non-negatives
mod_coeff_pos = []
mod_coeff_pos_err = 0.0

def func_hppm( input_data, 
              KW, t_coeff, base, 
              core_KW, 
              rest_cpu_KW,
              cpu_load, 
              gpu_load, 
              mem_load,
              l1l2, 
              l2ram, 
              actmon,
              inst0,
              inst1,
              inst2,
              inst3,
              inst4,
              inst5,
              inst6,
              inst7,
              gpu_ccl,
              gpu_int,
              gpu_f32,
              gpu_f64,
              gpu_misc,
              gpu_ctrl,
              gpu_cnv,
              gpu_l2r,
              gpu_l2w,
              gpu_l1,
              gpu_ldst,
              gpu_f16,
              gpu_shr,
              gpu_shw
                   ):
    
    global mod_coeff_pos
    global mod_coeff_pos_err
        
    global calc_component_power
    
    global mod_otherleak
    global mod_gpuleak
    global mod_gpufreq
    
    global mod_gpu_int
    global mod_gpu_f16
    global mod_gpu_f32
    global mod_gpu_f64
    global mod_gpu_misc
    global mod_gpu_ctrl
    global mod_gpu_cnv
    global mod_gpu_l2r
    global mod_gpu_l2w
    global mod_gpu_l1
    global mod_gpu_ldst
    global mod_gpu_ccl
    global mod_gpu_shr
    global mod_gpu_shw
    
    global mod_cpu_inst
    global mod_cpu_ccl
    global mod_cpu_leak
    
    
    min_vec = np.array( -1 )
    
    core_t_coeff = t_coeff
    rest_cpu_t_coeff = t_coeff
    
    s_t = input_data[0]
    s_gpuv = input_data[1]
    s_corev = input_data[2]
    s_cpuv = input_data[3]
    s_cpuccl = input_data[4]
    s_gpuccl = input_data[5]
    s_memccl = input_data[6]
    s_l1l2   = input_data[7]
    s_l2ram   = input_data[8]
    s_actmon   = input_data[9]
    
    s_cpuinst0 = input_data[10]
    if calc_component_power:    
        s_gpu_ccl  = input_data[11]
        s_gpu_int  = input_data[12]
        s_gpu_f32  = input_data[13]
        s_gpu_f64  = input_data[14]
        s_gpu_misc = input_data[15]
        s_gpu_ctrl = input_data[16]
        s_gpu_cnv  = input_data[17]
        s_gpu_l2r  = input_data[18]
        s_gpu_l2w  = input_data[19]
        s_gpu_l1   = input_data[20]
        s_gpu_ldst = input_data[21]
        s_cores    = input_data[22]
        s_gpu_f16  = input_data[23]
        s_gpu_shr  = input_data[24]
        s_gpu_shw  = input_data[25]
        s_power    = input_data[26]       
    else:
        s_cpuinst1 = input_data[11]
        s_cpuinst2 = input_data[12]
        s_cpuinst3 = input_data[13]
        s_cpuinst4 = input_data[14]
        s_cpuinst5 = input_data[15]
        s_cpuinst6 = input_data[16]
        s_cpuinst7 = input_data[17]
    
        s_gpu_ccl  = input_data[18]
        s_gpu_int  = input_data[19]
        s_gpu_f32  = input_data[20]
        s_gpu_f64  = input_data[21]
        s_gpu_misc = input_data[22]
        s_gpu_ctrl = input_data[23]
        s_gpu_cnv  = input_data[24]
        s_gpu_l2r  = input_data[25]
        s_gpu_l2w  = input_data[26]
        s_gpu_l1   = input_data[27]
        s_gpu_ldst = input_data[28]
        s_cores    = input_data[29]
        s_gpu_f16  = input_data[30]
        s_gpu_shr  = input_data[31]
        s_gpu_shw  = input_data[32]
        s_power    = input_data[33]
    
    num_values = len(s_t)
    ones = np.ones( num_values )

    res = base * ones
    print_dbg_mod("After BASE", res)
    
    core  = (core_KW * np.exp(-1/(np.array(t_coeff) * s_t)))
    res = res + core
    print_dbg_mod("After OTHER LEAK", res)
    
    cpu  = s_cpuv *                     (rest_cpu_KW * np.exp(-1/(np.array(t_coeff) * s_t)))
    res = res + cpu
    print_dbg_mod("After CPU LEAK", res)

    gpu  = s_gpuv *                     (KW          * np.exp(-1/(np.array(t_coeff) * s_t)))
    res = res + gpu
    print_dbg_mod("After GPU LEAK", res)
    
    res = res + (np.array(s_cpuccl) * cpu_load) * 1
    print_dbg_mod("After CPU load", res)
    res = res + (np.array(s_gpuccl) * gpu_load) * 1
    print_dbg_mod("After GPU load", res)
    res = res + (np.array(s_memccl) * mem_load) * 1
    print_dbg_mod("After MEM load", res)
    #res = res + (np.array(s_l1l2) * l1l2) * 0
    #res = res + (np.array(s_l2ram) * l2ram) * 0
    
    res = res + (np.array(s_actmon) * actmon) * 1
    print_dbg_mod("After ACTMON load", res)
    
    res = res + (np.array(s_cpuinst0) * inst0) * 1
    print_dbg_mod("After CPUINST load", res)
    if not calc_component_power:
        res = res + (np.array(s_cpuinst1) * inst0) * 1
        res = res + (np.array(s_cpuinst2) * inst0) * 1
    
    print_dbg_mod("Before GPU entry", res)
    res = res + (np.array(s_gpu_int)  * gpu_int) * 1
    print_dbg_mod("Before F16", res)
    res = res + (np.array(s_gpu_f16)  * gpu_f16) * 1
    print_dbg_mod("Before F32", res)
    res = res + (np.array(s_gpu_f32)  * gpu_f32) * 1
    print_dbg_mod("Before F64", res)
    res = res + (np.array(s_gpu_f64)  * gpu_f64) * 1
    print_dbg_mod("Before MISC", res)
    res = res + (np.array(s_gpu_misc) * gpu_misc) * 1
    print_dbg_mod("Before CTRL", res)
    res = res + (np.array(s_gpu_ctrl) * gpu_ctrl) * 1
    print_dbg_mod("Before CNV", res)
    res = res + (np.array(s_gpu_cnv)  * gpu_cnv) * 1
    print_dbg_mod("Before L2R", res)
    res = res + (np.array(s_gpu_l2r)  * gpu_l2r) * 1
    print_dbg_mod("Before L2W", res)
    res = res + (np.array(s_gpu_l2w)  * gpu_l2w) * 1
    print_dbg_mod("Before L1", res)
    res = res + (np.array(s_gpu_l1)   * gpu_l1)  * 1
    print_dbg_mod("Before LDST", res)
    res = res + (np.array(s_gpu_ldst) * gpu_ldst) * 0
    print_dbg_mod("Before CCL", res)
    res = res + (np.array(s_gpu_ccl)  * gpu_ccl) * 1
    print_dbg_mod("Before SHR", res)
    res = res + (np.array(s_gpu_shr)) * gpu_shr * 0
    print_dbg_mod("Before SHW", res)
    res = res + (np.array(s_gpu_shw)) * gpu_shw * 1
    print_dbg_mod("Final", res)
    
    """
    Check if all positive
    """    
    coeffs = np.array([KW, t_coeff, base, 
              core_KW, #core_t_coeff, 
              rest_cpu_KW, #rest_cpu_t_coeff,
              cpu_load, 
              gpu_load, 
              mem_load,
              actmon,
              inst0])
    
    if calc_component_power:
        mod_gpuleak = s_gpuv * (KW          * np.exp(-1/(np.array(t_coeff) * s_t)))
        mod_gpufreq = (np.array(s_gpuccl) * gpu_load)
        mod_gpu_int = (np.array(s_gpu_int)  * gpu_int)
        mod_gpu_f16 = (np.array(s_gpu_f16)  * gpu_f16)
        mod_gpu_f32 = (np.array(s_gpu_f32)  * gpu_f32)
        mod_gpu_f64 = (np.array(s_gpu_f64)  * gpu_f64)
        mod_gpu_misc = (np.array(s_gpu_misc) * gpu_misc)
        mod_gpu_ctrl = (np.array(s_gpu_ctrl) * gpu_ctrl)
        mod_gpu_cnv = (np.array(s_gpu_cnv) * gpu_cnv)
        mod_gpu_l2r = (np.array(s_gpu_l2r) * gpu_l2r)
        mod_gpu_l2w = (np.array(s_gpu_l2w) * gpu_l2w)
        mod_gpu_l1 = (np.array(s_gpu_l1) * gpu_l1)
        mod_gpu_shw = (np.array(s_gpu_shw) * gpu_shw)
        
        mod_cpu_leak = s_cpuv * (rest_cpu_KW * np.exp(-1/(np.array(t_coeff) * s_t)))
        mod_cpu_ccl  = np.array(s_cpuccl) * cpu_load
        mod_cpu_inst = (np.array(s_cpuinst0) * inst0) * 1
        #print core
        
        mod_otherleak = core
        
        return res
    
    err = np.power((s_power - res), 2)
    err = np.sum(err)
    err = np.sqrt(err)
    
    if (coeffs >= 0.0).all() and not calc_component_power:
        
        coeffs = np.array([KW, t_coeff, base, 
          core_KW, 
          rest_cpu_KW,
          cpu_load, 
          gpu_load, 
          mem_load,
          l1l2, 
          l2ram, 
          actmon,
          inst0,
          inst1,
          inst2,
          inst3,
          inst4,
          inst5,
          inst6,
          inst7,
          gpu_ccl,
          gpu_int,
          gpu_f32,
          gpu_f64,
          gpu_misc,
          gpu_ctrl,
          gpu_cnv,
          gpu_l2r,
          gpu_l2w,
          gpu_l1,
          gpu_ldst,
          gpu_f16,
          gpu_shr,
          gpu_shw])
        
        print "All-positive coeffs error: %s" % (err)
        #print coeffs

        mod_coeff_pos = coeffs
        mod_coeff_pos_err = err
    
    #err = np.sum(np.abs(s_power - res))
    
    #err 
    
    return err

def func_hppm_gpu( input_data, 
              KW, t_coeff, base, 
              core_KW, 
              rest_cpu_KW,
              cpu_load, 
              gpu_load, 
              mem_load,
              l1l2, 
              l2ram, 
              actmon,
              inst0,
              inst1,
              inst2,
              inst3,
              inst4,
              inst5,
              inst6,
              inst7,
              gpu_ccl,
              gpu_int,
              gpu_f32,
              gpu_f64,
              gpu_misc,
              gpu_ctrl,
              gpu_cnv,
              gpu_l2r,
              gpu_l2w,
              gpu_l1,
              gpu_ldst,
              gpu_f16,
              gpu_shr,
              gpu_shw
                   ):
    
    global mod_coeff_pos
    global mod_coeff_pos_err
        
    global calc_component_power
    
    global mod_otherleak
    global mod_gpuleak
    global mod_gpufreq
    
    global mod_gpu_int
    global mod_gpu_f16
    global mod_gpu_f32
    global mod_gpu_f64
    global mod_gpu_misc
    global mod_gpu_ctrl
    global mod_gpu_cnv
    global mod_gpu_l2r
    global mod_gpu_l2w
    global mod_gpu_l1
    global mod_gpu_ldst
    global mod_gpu_ccl
    global mod_gpu_shr
    global mod_gpu_shw
    
    global mod_cpu_inst
    global mod_cpu_ccl
    global mod_cpu_leak
    
    
    min_vec = np.array( -1 )
    
    core_t_coeff = t_coeff
    rest_cpu_t_coeff = t_coeff
    
    s_t = input_data[0]
    s_gpuv = input_data[1]
    s_corev = input_data[2]
    s_cpuv = input_data[3]
    s_cpuccl = input_data[4]
    s_gpuccl = input_data[5]
    s_memccl = input_data[6]
    s_l1l2   = input_data[7]
    s_l2ram   = input_data[8]
    s_actmon   = input_data[9]
    
    s_cpuinst0 = input_data[10]
    if calc_component_power:    
        s_gpu_ccl  = input_data[11]
        s_gpu_int  = input_data[12]
        s_gpu_f32  = input_data[13]
        s_gpu_f64  = input_data[14]
        s_gpu_misc = input_data[15]
        s_gpu_ctrl = input_data[16]
        s_gpu_cnv  = input_data[17]
        s_gpu_l2r  = input_data[18]
        s_gpu_l2w  = input_data[19]
        s_gpu_l1   = input_data[20]
        s_gpu_ldst = input_data[21]
        s_cores    = input_data[22]
        s_gpu_f16  = input_data[23]
        s_gpu_shr  = input_data[24]
        s_gpu_shw  = input_data[25]
        s_power    = input_data[26]       
    else:
        s_cpuinst1 = input_data[11]
        s_cpuinst2 = input_data[12]
        s_cpuinst3 = input_data[13]
        s_cpuinst4 = input_data[14]
        s_cpuinst5 = input_data[15]
        s_cpuinst6 = input_data[16]
        s_cpuinst7 = input_data[17]
    
        s_gpu_ccl  = input_data[18]
        s_gpu_int  = input_data[19]
        s_gpu_f32  = input_data[20]
        s_gpu_f64  = input_data[21]
        s_gpu_misc = input_data[22]
        s_gpu_ctrl = input_data[23]
        s_gpu_cnv  = input_data[24]
        s_gpu_l2r  = input_data[25]
        s_gpu_l2w  = input_data[26]
        s_gpu_l1   = input_data[27]
        s_gpu_ldst = input_data[28]
        s_cores    = input_data[29]
        s_gpu_f16  = input_data[30]
        s_gpu_shr  = input_data[31]
        s_gpu_shw  = input_data[32]
        s_power    = input_data[33]
    
    
    num_values = len(s_t)
    ones = np.ones( num_values )
    res = 0 * ones
   
    print_dbg_mod("Before GPU entry", res)
    res = res + (np.array(s_gpu_int)  * gpu_int) * 1
    print_dbg_mod("Before F16", res)
    res = res + (np.array(s_gpu_f16)  * gpu_f16) * 1
    print_dbg_mod("Before F32", res)
    res = res + (np.array(s_gpu_f32)  * gpu_f32) * 1
    print_dbg_mod("Before F64", res)
    res = res + (np.array(s_gpu_f64)  * gpu_f64) * 1
    print_dbg_mod("Before MISC", res)
    res = res + (np.array(s_gpu_misc) * gpu_misc) * 1
    print_dbg_mod("Before CTRL", res)
    res = res + (np.array(s_gpu_ctrl) * gpu_ctrl) * 1
    print_dbg_mod("Before CNV", res)
    res = res + (np.array(s_gpu_cnv)  * gpu_cnv) * 1
    print_dbg_mod("Before L2R", res)
    res = res + (np.array(s_gpu_l2r)  * gpu_l2r) * 1
    print_dbg_mod("Before L2W", res)
    res = res + (np.array(s_gpu_l2w)  * gpu_l2w) * 1
    print_dbg_mod("Before L1", res)
    res = res + (np.array(s_gpu_l1)   * gpu_l1)  * 1
    print_dbg_mod("Before LDST", res)
    res = res + (np.array(s_gpu_ldst) * gpu_ldst) * 0
    print_dbg_mod("Before CCL", res)
    res = res + (np.array(s_gpu_ccl)  * gpu_ccl) * 1
    print_dbg_mod("Before SHR", res)
    res = res + (np.array(s_gpu_shr)) * gpu_shr * 0
    print_dbg_mod("Before SHW", res)
    res = res + (np.array(s_gpu_shw)) * gpu_shw * 1
    print_dbg_mod("Final", res)
    
    """
    Check if all positive
    """    
    coeffs = np.array([
              gpu_ccl,
              gpu_int,
              gpu_f32,
              gpu_f64,
              gpu_misc,
              gpu_ctrl,
              gpu_cnv,
              gpu_l2r,
              gpu_l2w,
              gpu_l1,
              gpu_f16,
              gpu_shw])
    
    if calc_component_power:
        mod_gpuleak = s_gpuv * (KW          * np.exp(-1/(np.array(t_coeff) * s_t)))
        mod_gpufreq = (np.array(s_gpuccl) * gpu_load)
        mod_gpu_int = (np.array(s_gpu_int)  * gpu_int)
        mod_gpu_f16 = (np.array(s_gpu_f16)  * gpu_f16)
        mod_gpu_f32 = (np.array(s_gpu_f32)  * gpu_f32)
        mod_gpu_f64 = (np.array(s_gpu_f64)  * gpu_f64)
        mod_gpu_misc = (np.array(s_gpu_misc) * gpu_misc)
        mod_gpu_ctrl = (np.array(s_gpu_ctrl) * gpu_ctrl)
        mod_gpu_cnv = (np.array(s_gpu_cnv) * gpu_cnv)
        mod_gpu_l2r = (np.array(s_gpu_l2r) * gpu_l2r)
        mod_gpu_l2w = (np.array(s_gpu_l2w) * gpu_l2w)
        mod_gpu_l1 = (np.array(s_gpu_l1) * gpu_l1)
        mod_gpu_shw = (np.array(s_gpu_shw) * gpu_shw)
        
        #mod_cpu_leak = s_cpuv * (rest_cpu_KW * np.exp(-1/(np.array(t_coeff) * s_t)))
        #mod_cpu_ccl  = np.array(s_cpuccl) * cpu_load
        #mod_cpu_inst = (np.array(s_cpuinst0) * inst0) * 1
        #print core
        
        #mod_otherleak = core
        
        return res
    
    err = np.power((s_power - res), 2)
    err = np.sum(err)
    err = np.sqrt(err)
    
    if (coeffs >= 0.0).all() and not calc_component_power:
        
        coeffs = np.array([KW, t_coeff, base, 
          core_KW, 
          rest_cpu_KW,
          cpu_load, 
          gpu_load, 
          mem_load,
          l1l2, 
          l2ram, 
          actmon,
          inst0,
          inst1,
          inst2,
          inst3,
          inst4,
          inst5,
          inst6,
          inst7,
          gpu_ccl,
          gpu_int,
          gpu_f32,
          gpu_f64,
          gpu_misc,
          gpu_ctrl,
          gpu_cnv,
          gpu_l2r,
          gpu_l2w,
          gpu_l1,
          gpu_ldst,
          gpu_f16,
          gpu_shr,
          gpu_shw])
        
        print "All-positive coeffs error: %s" % (err)
        #print coeffs

        mod_coeff_pos = coeffs
        mod_coeff_pos_err = err
    
    #err = np.sum(np.abs(s_power - res))
    
    #err 
    
    return err

def get_last_positive():
    return [mod_coeff_pos, mod_coeff_pos_err]

def calc_cpu_rail_power():
    global mod_cpu_leak
    global mod_cpu_ccl
    global mod_cpu_inst
    
    #ret = mod_cpu_inst
    #ret = mod_cpu_leak + mod_cpu_ccl
    ret = mod_cpu_leak + mod_cpu_ccl + mod_cpu_inst
    
    return ret

def calc_gpu_rail_power():
    global mod_gpuleak
    global mod_gpufreq
    
    global mod_gpu_int
    global mod_gpu_f16
    global mod_gpu_f32
    global mod_gpu_f64
    global mod_gpu_misc
    global mod_gpu_ctrl
    global mod_gpu_cnv
    global mod_gpu_l2r
    global mod_gpu_l2w
    global mod_gpu_l1
    global mod_gpu_ldst
    global mod_gpu_ccl
    global mod_gpu_shr
    global mod_gpu_shw
    
    ret = mod_gpuleak + mod_gpufreq + mod_gpu_int + mod_gpu_f16 + mod_gpu_f32 + mod_gpu_f64 + mod_gpu_misc + mod_gpu_ctrl + mod_gpu_cnv + mod_gpu_l2r + mod_gpu_l2w + mod_gpu_l1 + mod_gpu_shw
    #ret = mod_gpuleak
     
    
    return ret

def bar_gpu_pow():
    global mod_gpuleak
    global mod_gpufreq
    
    global mod_gpu_int
    global mod_gpu_f16
    global mod_gpu_f32
    global mod_gpu_f64
    global mod_gpu_misc
    global mod_gpu_ctrl
    global mod_gpu_cnv
    global mod_gpu_l2r
    global mod_gpu_l2w
    global mod_gpu_l1
    global mod_gpu_ldst
    global mod_gpu_ccl
    global mod_gpu_shr
    global mod_gpu_shw
    
    leak_avg = np.average(mod_gpuleak)
    ccl_avg  = np.average(mod_gpufreq)
    
    int_avg = np.average(mod_gpu_int)
    f16_avg = np.average(mod_gpu_f16)
    f32_avg = np.average(mod_gpu_f32)
    f64_avg = np.average(mod_gpu_f64)
    misc_avg = np.average(mod_gpu_misc)
    ctrl_avg = np.average(mod_gpu_ctrl)
    cnv_avg = np.average(mod_gpu_cnv)
    l2r_avg = np.average(mod_gpu_l2r)
    l2w_avg = np.average(mod_gpu_l2w)
    l1_avg = np.average(mod_gpu_l1)
    shw_avg = np.average(mod_gpu_shw)
    
    plt.figure()
    bottom = 0
    plt.bar(0, leak_avg, label="Leakage")
    bottom = bottom + leak_avg
    plt.bar(0, ccl_avg, bottom=bottom, label="Cycle")
    bottom = bottom + ccl_avg
    plt.bar(0, int_avg, bottom=bottom, label="Integer")
    bottom = bottom + int_avg
    plt.bar(0, f16_avg, bottom=bottom, label="Half-precision")
    bottom = bottom + f16_avg
    plt.bar(0, f32_avg, bottom=bottom, label="Normal precision")
    bottom = bottom + f32_avg
    plt.bar(0, f64_avg, bottom=bottom, label="Double precision")
    bottom = bottom + f64_avg
    plt.bar(0, misc_avg, bottom=bottom, label="Misc")
    bottom = bottom + misc_avg
    plt.bar(0, ctrl_avg, bottom=bottom, label="Ctrl")
    bottom = bottom + ctrl_avg
    plt.bar(0, cnv_avg, bottom=bottom, label="Conversion")
    bottom = bottom + cnv_avg
    plt.bar(0, l2r_avg, bottom=bottom, label="L2R")
    bottom = bottom + l2r_avg
    plt.bar(0, l2w_avg, bottom=bottom, label="L2W")
    bottom = bottom + l2w_avg
    plt.bar(0, l1_avg, bottom=bottom, label="L1")
    bottom = bottom + l1_avg
    plt.bar(0, shw_avg, bottom=bottom, label="SHW")
    bottom = bottom + shw_avg
    
    plt.xlim([-0.5, 1.5])
    plt.ylabel("Power [W]")
    plt.title("C63 Power Breakdown")
    plt.legend()

def get_leak_power():
    return [mod_gpuleak, mod_cpu_leak, mod_otherleak]


"""
    global mod_gpu_int
    global mod_gpu_f16
    global mod_gpu_f32
    global mod_gpu_f64
    global mod_gpu_misc
    global mod_gpu_ctrl
    global mod_gpu_cnv
    global mod_gpu_l2r
    global mod_gpu_l2w
    global mod_gpu_l1
    global mod_gpu_ldst
    global mod_gpu_ccl
    global mod_gpu_shr
    global mod_gpu_shw
"""

def plot_power_detailed():
    
    plt.figure()
    plt.plot(mod_gpu_ccl)
    plt.title("Cycle Power")
    
    plt.figure()
    plt.plot(mod_gpu_f16)
    plt.title("GPU F16 Power")
    
    plt.figure()
    plt.plot(mod_gpu_f32)
    plt.title("GPU F32 Power")
    
    plt.figure()
    plt.plot(mod_gpu_f64)
    plt.title("GPU F64 Power")
    
    plt.figure()
    plt.plot(mod_gpu_misc)
    plt.title("GPU Misc Power")
    
    plt.figure()
    plt.plot(mod_gpu_ctrl)
    plt.title("GPU Ctrl Power")
    
    plt.figure()
    plt.plot(mod_gpu_cnv)
    plt.title("GPU Cnv Power")
    
    plt.figure()
    plt.plot(mod_gpu_l2r)
    plt.title("GPU L2R Power")
    
    plt.figure()
    plt.plot(mod_gpu_l2w)
    plt.title("GPU L2W Power")
    
    plt.figure()
    plt.plot(mod_gpu_l1)
    plt.title("GPU L1R Power")
    
    plt.figure()
    plt.plot(mod_gpu_shw)
    plt.title("GPU SHW Power")
    

def common_estimate_power( input_data, popt ):
    argstring = "func_hppm(input_data"
    i = 0
    for e in popt:
        argstring = argstring + ", popt[%s]" % ( i )
        i = i + 1
        
    argstring = argstring + ")"
    
    global calc_component_power
    calc_component_power = True
    print argstring
    est_pow = eval(argstring)
    
    est_gpu_rail = calc_gpu_rail_power()
    #plt.figure()
    #plt.plot(est_gpu_rail, label="Estimated Power [W]")
    #plt.plot(g_gpu_pow, label="Measured Power [W]")
    #plt.legend(loc="lower left")
    #plt.title("GPU RAIL")
    
    est_cpu_rail = calc_cpu_rail_power()
    #plt.figure()
    #plt.plot(est_cpu_rail, label="Estimated Power [W]")
    #plt.plot(g_cpu_pow, label="Measured Power [W]")
    #plt.legend(loc="lower left")
    #plt.title("CPU RAIL")
    
    plt.figure()
    plt.plot(est_pow, label="Estimated Power [W]")
    plt.plot(g_power, label="Measured Power [W]")
    plt.legend(loc="lower left")
    plt.ylabel("Power [W]")
    plt.title("C63 Video Encoder")
    plt.show()

    #plot_power_detailed()
    #bar_gpu_pow()
    
    calc_component_power = False
    
    return [est_pow, est_cpu_rail, est_gpu_rail]




