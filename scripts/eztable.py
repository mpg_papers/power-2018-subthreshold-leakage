#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sun May 19 17:05:17 2019

@author: krisrst
"""
"""
              KW, t_coeff, base, 0
              core_KW, 3
              rest_cpu_KW, 4
              cpu_load,  5
              gpu_load,  6
              mem_load, 7
              l1l2, 8 
              l2ram, 9
              actmon, 10
              inst0, 11
              inst1,
              inst2,
              inst3,
              inst4,
              inst5,
              inst6,
              inst7,
              gpu_ccl, 19
              gpu_int,  20
              gpu_f32, 21
              gpu_f64, 22
              gpu_misc, 23
              gpu_ctrl, 24
              gpu_cnv, 25
              gpu_l2r, 26
              gpu_l2w, 27
              gpu_l1, 28
              gpu_ldst, 29
              gpu_f16, 30
              gpu_shr, 31
              gpu_shw 32
"""


def oosonice(t1, t2):
    
    print "\\begin{table*}[t!]"
    print "    \\begin{scriptsize}"
    print "        \\begin{center}"
    print "            \\begin{tabular}{| c | c | c | c | c | c |} \\hline"
    print "                Rail    & Predictor & Description                               & Coefficient & Value (Nano) & Value (X1) \\\\ \\hline"
    print "            \\multirow{4}{*}{CPU} "
    print "                    & $V_{hp,core} $    & Temp.-dependent leakage coefficient   & $\\omega_{hp}$     & $ %s $ & $ %s $ \\\\ \\cline{2-6}" %('{0:.2f}'.format(t1[4]), '{0:.2f}'.format(t2[4]))
    print "                    & $\\rho_{hp,clk}$   & Active cycles (any core)              & $C_{hp,clk}$      & $ %s \\frac{pC}{V} $ & $ %s \\frac{pC}{V} $ \\\\ \\cline{2-6}" % ('{0:.2f}'.format(t1[5]*1e12), '{0:.2f}'.format(t2[5]*1e12))
    print "                    & $\\rho_{hp,ipsa}$  & CPU instructions                      & $C_{com,ips}$     & $ %s \\frac{pC}{V} $ & $ %s \\frac{pC}{V}$ \\\\ \\cline{2-6}" % ('{0:.2f}'.format(t1[11]*1e12), '{0:.2f}'.format(t2[11]*1e12))
    print "            \\multirow{13}{*}{GPU} "
    print "                    & $V_{gpu}   $      & Temp.dependent leakage coefficient    & $\\omega_{gpu}$    & $ %s $ & $ %s $ \\\\ \\cline{2-6}" %('{0:.2f}'.format(t1[0]), '{0:.2f}'.format(t2[0]))
    print "                    & $\\rho_{gpu,clk}$ & Total clock cycles                     & $C_{gpu,clk}$     & $ %s \\frac{nC}{V} $ & $ %s \\frac{nC}{V} $ \\\\ \\cline{2-6}" % ('{0:.2f}'.format(t1[6]*1e9), '{0:.2f}'.format(t2[6]*1e9))
    print "                    & $\\rho_{gpu,L2R}$  & L2 cache sector reads                 & $C_{gpu,L2R}$     & $ %s \\frac{nC}{V} $ & $ %s \\frac{nC}{V} $ \\\\ \\cline{2-6}" % ('{0:.2f}'.format(t1[26]*1e12), '{0:.2f}'.format(t2[26]*1e12))
    print "                    & $\\rho_{gpu,L2W}$  & L2 cache sector writes                & $C_{gpu,L2W}$     & $ %s \\frac{nC}{V} $ & $ %s \\frac{nC}{V} $\\\\ \\cline{2-6}" % ('{0:.2f}'.format(t1[27]*1e12), '{0:.2f}'.format(t2[27]*1e12))
    print "                    & $\\rho_{gpu,L1R}$  & L1 cache sector reads                 & $C_{gpu,L1R}$     & $ %s \\frac{pC}{V} $ & $ %s \\frac{pC}{V} $ \\\\ \\cline{2-6}" % ('{0:.2f}'.format(t1[28]*1e12), '{0:.2f}'.format(t2[28]*1e12))
    print "                    & $\\rho_{gpu,L1W}$  & L1 cache sector writes                & $C_{gpu,SHW}$     & $ %s \\frac{pC}{V} $ & $ %s \\frac{pC}{V} $ \\\\ \\cline{2-6}" % ('{0:.2f}'.format(t1[32]*1e12), '{0:.2f}'.format(t2[32]*1e12))
    print "                    & $\\rho_{gpu,INT}$  & Integer instructions                  & $C_{gpu,INT}$     & $ %s \\frac{pC}{V} $ & $ %s \\frac{pC}{V} $ \\\\ \\cline{2-6}" % ('{0:.2f}'.format(t1[20]*1e12), '{0:.2f}'.format(t2[20]*1e12))
    print "                    & $\\rho_{gpu,F16}$  & Float (16-bit) instructions           & $C_{gpu,F16}$     & $ %s \\frac{pC}{V} $ & $ %s \\frac{pC}{V} $ \\\\ \\cline{2-6}" % ('{0:.2f}'.format(t1[30]*1e12), '{0:.2f}'.format(t2[30]*1e12))
    print "                    & $\\rho_{gpu,F32}$  & Float (32-bit) instructions           & $C_{gpu,F32}$     & $ %s \\frac{pC}{V} $ & $ %s \\frac{pC}{V} $ \\\\ \\cline{2-6}" % ('{0:.2f}'.format(t1[21]*1e12), '{0:.2f}'.format(t2[21]*1e12))
    print "                    & $\\rho_{gpu,F64}$  & Float (64-bit) instructions           & $C_{gpu,F64}$     & $ %s \\frac{pC}{V} $ & $ %s \\frac{pC}{V} $ \\\\ \\cline{2-6}" % ('{0:.2f}'.format(t1[22]*1e12), '{0:.2f}'.format(t2[22]*1e12))
    print "                    & $\\rho_{gpu,CNV}$  & Conversion instructions               & $C_{gpu,CNV}$     & $ %s \\frac{pC}{V} $ & $ %s \\frac{pC}{V} $  \\\\ \\cline{2-6}" % ('{0:.2f}'.format(t1[25]*1e12), '{0:.2f}'.format(t2[25]*1e12))
    print "                    & $\\rho_{gpu,MSC}$  & Miscellaneous instructions            & $C_{gpu,MSC}$     & $ %s \\frac{pC}{V} $ & $ %s \\frac{pC}{V} $ \\\\ \\cline{2-6}" % ('{0:.2f}'.format(t1[23]*1e12), '{0:.2f}'.format(t2[23]*1e12))
    print "                    & $\\rho_{gpu,CTRL}$ & Control instructions                  & $C_{gpu,CTRL}$    & $ %s \\frac{pC}{V} $ & $ %s \\frac{pC}{V} $ \\\\ \\hline" % ('{0:.2f}'.format(t1[24]*1e12), '{0:.2f}'.format(t2[24]*1e12))
    print "            \\multirow{2}{*}{Memory}"
    print "                    & $\\rho_{mem,clk}$      & Clock cycles                      & $C_{mem,clk}$     & $ %s \\frac{pC}{V} $ & $ %s \\frac{pC}{V} $  \\\\ \\cline{2-6}" % ('{0:.2f}'.format(t1[7]*1e12), '{0:.2f}'.format(t2[7]*1e12))
    print "                    & $\\rho_{mem,act}$      & Active memory cycles              & $C_{mem,cpu}$     & $ %s \\frac{nC}{V} $ & $ %s \\frac{nC}{V} $  \\\\ \\hline" % ('{0:.2f}'.format(t1[10]*1e12), '{0:.2f}'.format(t2[10]*1e12))
    print "            \\multirow{2}{*}{Other}"
    print "                    & $T_{soc}   $          & Thermal coefficient               & $\\alpha_{soc}$    & $ %s 10^{-3} $ & $ %s 10^{-3}$  \\\\ \\cline{2-6}" % ('{0:.2f}'.format(t1[1]*1e3), '{0:.2f}'.format(t2[1]*1e3))
    print "                    &                       & Temp.-dependent leakage coefficient   & $\\omega_{other}$     & $ %s $ & $ %s $ \\\\ \\cline{2-6}" %('{0:.2f}'.format(t1[3]), '{0:.2f}'.format(t2[3]))
    print "                    & $P_{base}$            & Base power                        & 1                 & $ %s mW$ & $ %s mW$ \\\\ \\hline" % ('{0:.2f}'.format(t1[2]*1e3), '{0:.2f}'.format(t2[2]*1e3))
    print "            \\end{tabular}"
    print "            \\caption{\\label{tab:model}Estimated model coefficients.}"
    print "        \\end{center}"
    print "%        \\vspace{-0.7cm}"
    print "    \\end{scriptsize}"
    print "\\end{table*}"
