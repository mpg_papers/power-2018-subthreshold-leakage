#! /usr/bin/python


# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import pandas as pd
import numpy  as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d, Axes3D
from numpy import linspace, meshgrid
from matplotlib.mlab import griddata
import matplotlib
#import scipy.optimize as optimize
#from scipy.optimize import fsolve, root
from scipy.optimize import curve_fit

font = {'size'   : 30}

matplotlib.rc('font', **font)

temp_t20 = 25.0E-3
temp_coeff = 3.0E-3
def func_temp( temp, coeff ):
    
    _d = ((temp * coeff) + temp_t20)
    return _d
    
# May vary depending on rail
v_thresh = 0.8

# Initial guess
guess_KW = 50.0
guess_t_c = 3.0E-3
guess_base = 3.8

# Bounds
bound_KW  = (30, 60)
bound_t_c = (1.0E-3, 5.0E-3)
bound_base = (3.0, 5.0)

global_volts = []
global_temps = []
global_powers = []
global_est_powers = []
cur_iteration = 0

colors = ['#f41de2', '#a54bf4', '#4aa5f4', '#25aa63', '#220988', 'c', 'g', 'b', 'r', 'k', ]

fig = plt.figure()
ax = fig.add_subplot(111)

def func_leak( input_data, KW, t_coeff, base ):
    
    min_vec = ( -1 )
    
    s_t = input_data[0]
    s_v = input_data[1]
    
    e1 = np.exp( (min_vec * v_thresh ) / func_temp( s_t, t_coeff ))
    e2 = np.exp( (min_vec * s_v )      / func_temp( s_t, t_coeff ))
    
#    r_e1 = np.exp( (min_vec * v_thresh ) / func_temp( s_t, rest_t_coeff ))
#    r_e2 = np.exp( (min_vec * 1.0 )      / func_temp( s_t, rest_t_coeff ))
    
    res = ((e1 - ( e2 * e1 ) ) * KW * s_v) + base
#    res = ((r_e1 - ( r_e2 * r_e1 ) ) * rest_KW) + ((e1 - ( e2 * e1 ) ) * KW * s_v) + base
    
    return res


def plot_grouped_volt( temps, volts, powers, min_volt, max_volt ):
    
    global global_temps
    global global_volts
    global global_powers
    global cur_iteration

    cur_data = []

    for e in zip(temps, volts, powers):
        if (e[1] >= min_volt) and (e[1] < max_volt):
            cur_data.append(e)

    t_, v_, p_ = zip(*cur_data)
        
    input_data = [t_, v_]
    initial_guess = [ guess_KW, guess_t_c, guess_base ]
    popt, pcov = curve_fit(func_leak, input_data, p_, initial_guess)
        
    print "popt: %s" % (popt)
    est_KW = popt[0]
    est_t_c = popt[1]
    est_base = popt[2]
    
    ax.scatter(t_, p_, color=colors[cur_iteration], marker='+', alpha=0.8 ) # Color map : c=v_
    
    return [est_KW, est_t_c, est_base]
#        est_rest_KW = popt[3]
#        est_rest_t_c = popt[4]
        
        #test_pow = [x for x in func_leak( [test_temp, test_volt], est_KW, est_t_c, est_base )]
        
        # For 2D scatter
        #ax.scatter(t_, p_, color=colors[i], marker='+', alpha=0.8 ) # Color map : c=v_
        #ax.plot(ptemps, ppower, color=colors[i], lw=2.0)
        #ax.plot( test_temp, test_pow, color=colors[i], lw=2.0, label="%.2f V - %.2f V" % (cur_v, next_v))
        #ax.scatter(t_, p_, i, color=colors[i], alpha=0.8, label="%s V - %s V" % (cur_v, next_v))
        # ax.bar(t_, p_, i, alpha=0.8, color=colors[i])

        #data.append( cur_data )

f_l2r = pd.read_csv('/home/krisrst/test_results/gpu_mod/l2r/all.csv')
f_int = pd.read_csv('/home/krisrst/test_results/gpu_mod/int/all.csv')
f_f32 = pd.read_csv('/home/krisrst/test_results/gpu_mod/f32/all.csv')
f_idle = pd.read_csv('/home/krisrst/test_results/leak_idle/1/all.csv')


"""
"""
"""
"""
min_volt = 0.8
max_volt = 0.86
test_temp = np.linspace( 20, 90)
test_volt = np.ones( len(test_temp) ) * (0.83)

def plot_subthreshold(name, f):
    
    global cur_iteration
    
    volt    = f['VDD1']
    temp_mem = f['TEMP_MEM']
    temp_pll = f['TEMP_PLL']
    temp_gpu = f['TEMP_GPU']
    temp_cpu = f['TEMP_CPU']
    temp    = (temp_mem + temp_pll + temp_gpu + temp_cpu) / 4
    power   = f['POWER']

    est_KW, est_t_c, est_base = plot_grouped_volt( temp, volt, power, min_volt, max_volt )
    print "Estimates for %s : KW %s - t_c %s - base %s" % (name, est_KW, est_t_c, est_base)
    test_pow = [x for x in func_leak( [test_temp, test_volt], est_KW, est_t_c, est_base )]

    ax.plot( test_temp, test_pow, color=colors[cur_iteration], lw=2.0, label=name)
    
    cur_iteration = cur_iteration + 1


plot_subthreshold("IDLE", f_idle)
plot_subthreshold("L2R", f_l2r)
plot_subthreshold("INT", f_int)
plot_subthreshold("F32", f_f32)


plt.xlabel("Average Temperature [C]")
plt.ylabel("Power [W]")

plt.legend(loc="upper left", title="GPU Benchmark")
plt.show()
