#! /usr/bin/python


# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import pandas as pd
import numpy  as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d, Axes3D
from numpy import linspace, meshgrid
from matplotlib.mlab import griddata
import matplotlib
#import scipy.optimize as optimize
#from scipy.optimize import fsolve, root
from scipy.optimize import curve_fit

font = {'size'   : 10} # 30

matplotlib.rc('font', **font)

temp_t20 = 25.0E-3
temp_coeff = 3.0E-3
def func_temp( temp, coeff ):
    
    _d = ((temp * coeff) + temp_t20)
    return _d


g_temp_avg = []
g_hpv = []
g_corev = []
g_gpuv = []
g_memv = []
g_cores = []
g_cpufrq_p = []
g_gpufrq_p = []
g_memfrq_p = []
g_com_l1l2_p = []
g_com_l2ram_p = []
g_mem_actmon_p = []
g_power = []
g_cpuinst_idle_cpu = []
g_cpuinst_idle_gpu = []
g_cpuinst_idle_core = []
g_cpuinst_int = []
g_cpu_l1drf_tot = []
g_cpu_l1irf_tot = []
g_cpu_l2drf_tot = []
g_cpu_l2dwb_tot = []

g_gpu_int = []
g_gpu_f32 = []
g_gpu_f64 = []
g_gpu_misc = []
g_gpu_ctrl = []
g_gpu_cnv = []
g_gpu_l2r = []
g_gpu_l2w = []
g_gpu_l1 = []
g_gpu_ldst = []
g_gpu_ccl = []

g_cpuinst = []
g_depth = 0

def add_data( f ):
    
    global g_temp_avg
    global g_hpv
    global g_corev
    global g_gpuv
    global g_memv
    global g_cores
    global g_cpufrq_p
    global g_gpufrq_p
    global g_memfrq_p
    global g_com_l1l2_p
    global g_com_l2ram_p
    global g_mem_actmon_p
    global g_cpuinst
    global g_depth
    global g_cpu_l1drf_tot
    global g_cpu_l1irf_tot
    global g_cpu_l2drf_tot
    global g_cpu_l2dwb_tot
    global g_gpu_ccl
    global g_gpu_int
    global g_gpu_f32
    global g_gpu_f64
    global g_gpu_misc
    global g_gpu_ctrl
    global g_gpu_cnv
    global g_gpu_l2r
    global g_gpu_l2w
    global g_gpu_l1
    global g_gpu_ldst

    ones = np.ones(len(f['Timestamp']))
    xs   = range(len(f['Timestamp']))
    dur = f['Duration'] / 1E3
    
    """
    Increase array size for cpuinsts
    """
    for e in g_cpuinst:
        e.extend( 0 * np.ones( len(ones) ))
    
    power = f['POWER']
    g_power.extend( power )
    
    """
    Platform voltages.
    """
    hpv = f['VDD2']
    gpuv = f['VDD1']
    corev = f['VDD_CORE']
    memv = ones * 1.1 # 1.1 V LPDDR4
    
    g_hpv.extend( hpv )
    g_corev.extend( corev )
    g_gpuv.extend( gpuv )
    g_memv.extend( memv )
    
    """
    Square platform voltages.
    """
    hpv2 = hpv * hpv
    gpuv2 = gpuv * gpuv
    corev2 = corev * corev
    memv2 = memv * memv
    
    """
    Leakage and Temperature
    """
    
    temp_mem = f['TEMP_MEM']
    temp_pll = f['TEMP_PLL']
    temp_gpu = f['TEMP_GPU']
    temp_cpu = f['TEMP_CPU']
    
    temp_avg = (temp_mem + temp_pll + temp_gpu + temp_cpu) / 4
    g_temp_avg.extend( temp_avg )
    
    core0 = ones
    core1 = f['CORE1']
    core2 = f['CORE2']
    core3 = f['CORE3']
    
    core_tot = core0 + core1 + core2 + core3
    g_cores.extend(core_tot)
    """
    GPU
    """
    gpu_ccl = f['ELAPSED_CYCLES_SM']
    gpu_int = f['INST_INTEGER']
    gpu_f32 = f['INST_FP_32']
    gpu_f64 = f['INST_FP_64']
    gpu_ctrl = f['INST_CONTROL']
    gpu_misc = f['INST_MISC']
    gpu_cnv = f['INST_BIT_CONVERT']
    gpu_ldst = f['INST_LD_ST']
    
    gpu_l2r = f['L2_SUBP0_READ_SECTOR_QUERIES']  + f['L2_SUBP1_READ_SECTOR_QUERIES']
    gpu_l2w = f['L2_SUBP0_WRITE_SECTOR_QUERIES'] + f['L2_SUBP1_WRITE_SECTOR_QUERIES']
    
    gpu_l1r = f['L1_TEX0_CACHE_SECTOR_QUERIES'] + f['L1_TEX1_CACHE_SECTOR_QUERIES']
    try:
        gpu_l1r -= f['L1_TEX0_CACHE_SECTOR_MISSES'] + f['L1_TEX1_CACHE_SECTOR_MISSES']
    except KeyError:
        print "WARNING: No l1 cache miss counter"
    
    gpu_ccl_p  = gpu_ccl  * gpuv2 / dur
    gpu_int_p  = gpu_int  * gpuv2 / dur
    gpu_f32_p  = gpu_f32  * gpuv2 / dur
    gpu_f64_p  = gpu_f64  * gpuv2 / dur
    gpu_ctrl_p = gpu_ctrl * gpuv2 / dur
    gpu_misc_p = gpu_misc * gpuv2 / dur
    gpu_cnv_p  = gpu_cnv  * gpuv2 / dur
    gpu_ldst_p = gpu_ldst * gpuv2 / dur
    gpu_l2r_p  = gpu_l2r  * gpuv2 / dur
    gpu_l2w_p  = gpu_l2w  * gpuv2 / dur
    gpu_l1_p   = gpu_l1r  * gpuv2 / dur
    
    g_gpu_ccl.extend(gpu_ccl_p)
    g_gpu_int.extend(gpu_int_p)
    g_gpu_f32.extend(gpu_f32_p)
    g_gpu_f64.extend(gpu_f64_p)
    g_gpu_misc.extend(gpu_misc_p)
    g_gpu_ctrl.extend(gpu_ctrl_p)
    g_gpu_cnv.extend(gpu_cnv_p)
    g_gpu_l2r.extend(gpu_l2r_p)
    g_gpu_l2w.extend(gpu_l2w_p)
    g_gpu_l1.extend(gpu_l1_p)
    g_gpu_ldst.extend(gpu_ldst_p)
    
    """
    Misc.
    """
    cpu_cycles_tot = f['CPU0_CYCLES'] + f['CPU1_CYCLES'] + f['CPU2_CYCLES'] + f['CPU3_CYCLES']
    cpu_inst_tot = f['CPU0_INST_EXEC_SW'] + f['CPU1_INST_EXEC_SW'] + f['CPU2_INST_EXEC_SW']+ f['CPU3_INST_EXEC_SW']
    cpu_l1drf_tot = f['CPU0_L1D_CACHE_RF'] + f['CPU1_L1D_CACHE_RF'] + f['CPU2_L1D_CACHE_RF'] + f['CPU3_L1D_CACHE_RF']
    cpu_l1irf_tot = f['CPU0_L1I_CACHE_RF'] + f['CPU1_L1I_CACHE_RF'] + f['CPU2_L1I_CACHE_RF'] + f['CPU3_L1I_CACHE_RF']
    cpu_l2drf_tot = f['CPU0_L2D_CACHE_RF'] + f['CPU1_L2D_CACHE_RF'] + f['CPU2_L2D_CACHE_RF'] + f['CPU3_L2D_CACHE_RF']
    cpu_l2dwb_tot = f['CPU0_L2D_CACHE_WB'] + f['CPU1_L2D_CACHE_WB'] + f['CPU2_L2D_CACHE_WB'] + f['CPU3_L2D_CACHE_WB']
    
    g_cpu_l1drf_tot.extend( (cpu_l1drf_tot - cpu_l2drf_tot + cpu_l1irf_tot) * hpv2 )
    g_cpu_l1irf_tot.extend( cpu_l1irf_tot * hpv2 )
    g_cpu_l2drf_tot.extend( cpu_l2drf_tot * hpv2 )
    g_cpu_l2dwb_tot.extend( cpu_l2dwb_tot * hpv2 )
    
    com_l1l2 = (cpu_l1drf_tot  ) + cpu_l2drf_tot
    com_l2ram = cpu_l2drf_tot + cpu_l2dwb_tot

    com_l1l2_p = com_l1l2 * hpv2
    com_l2ram_p = com_l2ram * hpv2
    g_com_l1l2_p.extend( com_l1l2_p / dur )
    g_com_l2ram_p.extend( com_l2ram_p / dur )
    
    cpu_inst_p = cpu_inst_tot * hpv2  / dur
    
    new_cpuinst = []
    new_cpuinst.extend(0 * np.ones( g_depth ))
    new_cpuinst.extend( cpu_inst_p )
    g_cpuinst.append( new_cpuinst )
    g_depth += len(ones)
    
    mem_actmon_p = f['ACTMON_ALL'] * memv2
    g_mem_actmon_p.extend( mem_actmon_p / dur )
    
    """
    Clock cycle predictors
    """
    gpufrq_p = f['GPUFREQ'] * gpuv2 * 1E3
    cpufrq_p = cpu_cycles_tot * hpv2
    memfrq_p = f['MEMFREQ'] * memv2 * 1E3
    
    g_cpufrq_p.extend(cpufrq_p / dur)
    g_gpufrq_p.extend(gpufrq_p)
    g_memfrq_p.extend(memfrq_p)
    
    """
    Return workload-specific
    """
    return cpu_inst_p
    
def func_leakp( input_data, 
              KW, t_coeff, base, 
              core_KW, #core_t_coeff, 
              rest_cpu_KW, #rest_cpu_t_coeff,
              gpuleak, hpleak, coreleak,
              gpu_load, cpu_load
                   ):
    
    min_vec = ( -1 )
    
    core_t_coeff = t_coeff
    rest_cpu_t_coeff = t_coeff
    
    s_t = input_data[0]
    s_gpuv = input_data[1]
    s_corev = input_data[2]
    s_cpuv = input_data[3]
    s_gpuccl = input_data[4]
    s_cpuccl = input_data[5]
    
    gpu_e1 = np.exp( (min_vec * v_thresh_gpu )   / func_temp( s_t, t_coeff ))
    gpu_e2 = np.exp( (min_vec * s_gpuv )         / func_temp( s_t, t_coeff ))
    
    core_e1 = np.exp( (min_vec * v_thresh_core ) / func_temp( s_t, core_t_coeff ))
    core_e2 = np.exp( (min_vec * s_corev )       / func_temp( s_t, core_t_coeff ))
    
    cpu_e1 = np.exp( (min_vec * v_thresh_cpu )   / func_temp( s_t, rest_cpu_t_coeff ))
    cpu_e2 = np.exp( (min_vec * s_cpuv )         / func_temp( s_t, rest_cpu_t_coeff ))
    
    print "Core leak power %s" % ((core_e1 - ( core_e2 * core_e1 ) ) * core_KW * s_corev)
    print "GPU leak power %s" % ((gpu_e1 - ( gpu_e2 * gpu_e1 ) ) * KW * s_gpuv)
    print "CPU leak power %s" % ((cpu_e1 - ( cpu_e2 * cpu_e1 ) ) * rest_cpu_KW * s_cpuv)
    
v_thresh_cpu = 0.8
v_thresh_gpu = 0.8
v_thresh_core = 0.8

def func_leak( input_data, 
              KW, t_coeff, base, 
              core_KW, #core_t_coeff, 
              rest_cpu_KW, #rest_cpu_t_coeff,
              gpuleak, hpleak, coreleak,
              gpu_load, cpu_load
                   ):
    
    min_vec = ( -1 )
    
    core_t_coeff = t_coeff
    rest_cpu_t_coeff = t_coeff
    
    s_t = input_data[0]
    s_gpuv   = input_data[1]
    s_corev  = input_data[2]
    s_cpuv   = input_data[3]
    s_gpuccl = input_data[4]
    s_cpuccl = input_data[5]
    cores    = input_data[6]
    
    gpu_e1 = np.exp( (min_vec * v_thresh_gpu )   / func_temp( s_t, t_coeff ))
    gpu_e2 = np.exp( (min_vec * s_gpuv )         / func_temp( s_t, t_coeff ))
    
    core_e1 = np.exp( (min_vec * v_thresh_core ) / func_temp( s_t, core_t_coeff ))
    core_e2 = np.exp( (min_vec * s_corev )       / func_temp( s_t, core_t_coeff ))
    
    cpu_e1 = np.exp( (min_vec * v_thresh_cpu )   / func_temp( s_t, rest_cpu_t_coeff ))
    cpu_e2 = np.exp( (min_vec * s_cpuv )         / func_temp( s_t, rest_cpu_t_coeff ))
    
    res = base
    res = res + (s_corev * core_KW) * 0
    res = res + ((core_e1 - ( core_e2 * core_e1 ) ) * core_KW * s_corev) * 0
    res = res + ((gpu_e1 - ( gpu_e2 * gpu_e1 ) ) * KW * cores * s_gpuv)
    res = res + ((cpu_e1 - ( cpu_e2 * cpu_e1 ) ) * rest_cpu_KW * s_cpuv)
    res = res + (s_gpuccl * gpu_load) * 1
    res = res + (s_cpuccl * cpu_load) * 1
    res = res + (s_gpuv  * s_gpuv * gpuleak) * 0
    res = res + (s_cpuv  * s_cpuv * hpleak) * 0
    res = res + (s_corev * s_corev * coreleak) * 0
    
    return res

#f_leak = pd.read_csv('/home/krisrst/test_results/leak_idle/3/all_cpu_core_gpu.csv')
f_leak = pd.read_csv('/home/krisrst/test_results/leak_idle/3/all_combined_lessgpuvolt.csv')
#f_range = pd.read_csv('/home/krisrst/test_results/leak_idle/freq_ranges/all_nogpuvolt.csv')
#f_gpu = pd.read_csv('/home/krisrst/test_results/hppm/gpu/all_caches_and_workloads.csv')
#f_cpu_int = pd.read_csv('/home/krisrst/test_results/hppm/cpu/int/all.csv')
#f_cpu_neon = pd.read_csv('/home/krisrst/test_results/hppm/cpu/neon/all.csv')
#f_cpu_vfp = pd.read_csv('/home/krisrst/test_results/hppm/cpu/vfp/all.csv')
#f_cpu_int_v = pd.read_csv('/home/krisrst/test_results/hppm/cpu/int_vol/all.csv')
#f_cpu_l1rf = pd.read_csv('/home/krisrst/test_results/hppm/cpu/l1rf/all.csv')
#f_cpu_l1wb = pd.read_csv('/home/krisrst/test_results/hppm/cpu/l1wb/all.csv')

# Idle CPU inst
leak_inst = add_data( f_leak )
leak_range = add_data( f_range )
#gpu_inst = add_data( f_gpu )

# CPU BMs
#cpu_int  = add_data( f_cpu_int )
#cpu_neon = add_data( f_cpu_neon )
#cpu_vfp = add_data( f_cpu_vfp )
#cpu_int_v = add_data( f_cpu_int_v )
#cpu_l1rf = add_data( f_cpu_l1rf )
#cpu_l1wb = add_data( f_cpu_l1wb )

# Sum up idle cpuinst
#g_cpuinst_prof = [sum(x) for x in zip(g_cpuinst[0], g_cpuinst[1], g_cpuinst[2])]

"""
leak_gpu  = add_data( f_leak_gpu )
zeros = 0 * np.ones(len(leak_gpu) )
g_cpuinst_idle_gpu.extend( zeros )
g_cpuinst_idle_cpu.extend( zeros )
g_cpuinst_idle_core.extend( zeros )
#g_cpuinst_int.extend( zeros )

leak_core = add_data( f_leak_core )
zeros = 0 * np.ones(len(leak_core) )
g_cpuinst_idle_gpu.extend( zeros )
g_cpuinst_idle_cpu.extend( zeros )
g_cpuinst_idle_core.extend( zeros )
#g_cpuinst_int.extend( zeros )

leak_cpu  = add_data( f_leak_cpu )
zeros = 0 * np.ones(len(leak_cpu) )
g_cpuinst_idle_gpu.extend( zeros )
g_cpuinst_idle_cpu.extend( zeros )
g_cpuinst_idle_core.extend( zeros )
#g_cpuinst_int.extend( zeros )

#int_cpu = add_data( f_cpu_int )
#zeros = 0 * np.ones(len(int_cpu) )
#g_cpuinst_idle_gpu.extend( zeros )
#g_cpuinst_idle_cpu.extend( zeros )
#g_cpuinst_idle_core.extend( zeros )
#g_cpuinst_int.extend( int_cpu )
"""

# Initial guess
guess_KW_gpu = 5.0
guess_KW_core = 5.0
guess_KW_cpu = 5.0
guess_t_c_gpu = 3E-3
guess_t_c_core = 3E-3
guess_t_c_cpu = 3E-3
guess_base = 3.0
guess_cpu_load = 300E-10
guess_gpu_load = 200E-10
guess_mem_load = 200E-10
guess_l1l2 = 20E-12
guess_l2ram = 20E-12
guess_actmon = 2E-9
guess_inst = 0.2E-9
guess_nano = 1E-12
guess_pico = 1E-12
guess_leak = 1
                   
    #global g_cpu_l1drf_tot
    #global g_cpu_l1irf_tot
    #global g_cpu_l2drf_tot
    #global g_cpu_l2dwb_tot


"""

    g_gpu_int
    g_gpu_f32
    g_gpu_f64
    g_gpu_misc
    g_gpu_ctrl
    g_gpu_cnv
    g_gpu_l2r
    g_gpu_l2w
    g_gpu_l1
    g_gpu_ldst

"""



ind = np.linspace(0, len(g_power), num=len(g_power))

if False:
    plt.subplot(2, 2, 1)
    plt.scatter(ind, g_hpv)
    plt.title("g_hpv")
    plt.subplot(2, 2, 2)
    plt.scatter(ind, g_corev)
    plt.title("g_corev")
    plt.subplot(2, 2, 3)
    plt.scatter(ind, g_gpuv)
    plt.title("g_gpuv")
    plt.subplot(2, 2, 4)
    plt.scatter(ind, g_gpufrq_p)
    plt.title("g_cpu_l2dwb_tot")

if True:
    
    # Prepare curve fit
    input_data      = [ g_temp_avg, g_gpuv, g_corev, g_hpv,
                       g_gpufrq_p, g_cpufrq_p, g_cores
                       ]
    
    names = ["guess_KW", "guess_t_c", "guess_base", 
                       "guess_KW", #"guess_t_c", 
                       "guess_KW", #"guess_t_c", 
                       "guess_leak", "guess_leak", "guess_leak", 
                       "guess_gpu_load", "guess_cpu_load"
                       ]
    #initial_guess = []
    #for e in names:
    #    cmd = "coeffs.append( %s )" % e
    #    exec(cmd)
    
    initial_guess   = [ guess_KW_gpu, guess_t_c_gpu, guess_base, 
                       guess_KW_core, #guess_t_c_core, 
                       guess_KW_cpu,  #guess_t_c_cpu,
                       guess_leak, guess_leak, guess_leak,
                       guess_pico, guess_pico
                       ]    
    
    popt, pcov = curve_fit(func_leak, input_data, g_power, initial_guess)
    for x in zip(names, popt):
        print "%s : %s" % (x[0], x[1])
        
    func_leakp([70, 0.8, 0.875, 0.84, 
               0.8*0.8*100E6, 
               0.84*0.84*100E6], 
               popt[0],popt[1],popt[2],popt[3],popt[4],popt[5],popt[6],popt[7],popt[8],popt[9]
               )