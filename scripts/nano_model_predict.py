#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Sat Nov 10 10:49:04 2018

@author: krisrst
"""

from sklearn import linear_model
import pandas as pd
import numpy  as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d, Axes3D
from numpy import linspace, meshgrid
from matplotlib.mlab import griddata
import matplotlib
import hppm
from figuretools import *
#import scipy.optimize as optimize
#from scipy.optimize import fsolve, root
from scipy.optimize import curve_fit

font = {'size'   : 10} # 30

matplotlib.rc('font', **font)

#f = pd.read_csv('/home/krisrst/power_models/nano/sensor_all_temp.csv')
#common.add_data( f )

# Workload testing
f = pd.read_csv('/home/krisrst/inf5063/nano_eval/1_h16/all.csv')
hppm.add_data( f )

f = pd.read_csv('/home/krisrst/inf5063/nano_eval/2_h16/all.csv')
hppm.add_data( f )

f = pd.read_csv('/home/krisrst/inf5063/nano_eval/1_h17/all.csv')
hppm.add_data( f )

hppm.extend_cpuinst()
hppm.merge_cpuinsts()
