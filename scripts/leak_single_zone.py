#! /usr/bin/python


# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import pandas as pd
import numpy  as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d, Axes3D
from numpy import linspace, meshgrid
from matplotlib.mlab import griddata
import matplotlib
#import scipy.optimize as optimize
#from scipy.optimize import fsolve, root
from scipy.optimize import curve_fit

font = {'size'   : 24}

matplotlib.rc('font', **font)

temp_t20 = 25.0E-3
temp_coeff = 3.0E-3
def func_temp( temp, coeff ):
    
    _d = ((temp * coeff) + temp_t20)
    return _d
    
# May vary depending on rail
v_thresh = 0.8

# Initial guess
guess_KW = 50.0
guess_t_c = 3.0E-3
guess_base = 3.8

# Bounds
bound_KW  = (30, 60)
bound_t_c = (1.0E-3, 5.0E-3)
bound_base = (3.0, 5.0)

global_volts = []
global_temps = []
global_powers = []
global_est_powers = []    

def func_leak( input_data, KW, t_coeff, base ):
    
    min_vec = ( -1 )
    
    s_t = input_data[0]
    s_v = input_data[1]
    
    e1 = np.exp( (min_vec * v_thresh ) / func_temp( s_t, t_coeff ))
    e2 = np.exp( (min_vec * s_v )      / func_temp( s_t, t_coeff ))
    
#    r_e1 = np.exp( (min_vec * v_thresh ) / func_temp( s_t, rest_t_coeff ))
#    r_e2 = np.exp( (min_vec * 1.0 )      / func_temp( s_t, rest_t_coeff ))
    
    res = ((e1 - ( e2 * e1 ) ) * KW * s_v) + base
#    res = ((r_e1 - ( r_e2 * r_e1 ) ) * rest_KW) + ((e1 - ( e2 * e1 ) ) * KW * s_v) + base
    
    return res


def plot_grouped_volt( temps, volts, powers, num_groups=10 ):
    
    global global_temps
    global global_volts
    global global_powers

    min_v = min(volts)
    max_v = max(volts)
    interval_v = (max_v - min_v) / num_groups

    print "Min v : %s max v : %s interval : %s" % (min_v, max_v, interval_v)

    data = []

    fig = plt.figure()
    ax = fig.add_subplot(111)
    colors = ['#f41de2', '#a54bf4', '#4aa5f4', '#25aa63', '#220988', 'c', 'g', 'b', 'r', 'k', ]

    for i in range( num_groups ):
        cur_v  = min_v + i * interval_v
        next_v = cur_v + interval_v

        cur_data = []

        for e in zip(temps, volts, powers):
            if (e[1] >= cur_v) and (e[1] < next_v):
                cur_data.append(e)

        t_, v_, p_ = zip(*cur_data)
        
        input_data = [t_, v_]
        initial_guess = [ guess_KW, guess_t_c, guess_base ]
        popt, pcov = curve_fit(func_leak, input_data, p_, initial_guess)
        
        print "popt: %s" % (popt)
        est_KW = popt[0]
        est_t_c = popt[1]
        est_base = popt[2]
#        est_rest_KW = popt[3]
#        est_rest_t_c = popt[4]
        
        test_temp = np.linspace( 20, 90)
        test_volt = np.ones( len(test_temp) ) * (cur_v + ( (next_v - cur_v ) / 2))
        
        test_pow = [x for x in func_leak( [test_temp, test_volt], est_KW, est_t_c, est_base )]
        
        # For 2D scatter
        ax.scatter(t_, p_, color=colors[i], marker='+', alpha=0.8 ) # Color map : c=v_
        #ax.plot(ptemps, ppower, color=colors[i], lw=2.0)
        ax.plot( test_temp, test_pow, color=colors[i], lw=2.0, label="%.2f V - %.2f V" % (cur_v, next_v))
        #ax.scatter(t_, p_, i, color=colors[i], alpha=0.8, label="%s V - %s V" % (cur_v, next_v))
        # ax.bar(t_, p_, i, alpha=0.8, color=colors[i])

        data.append( cur_data )
        
    plt.xlabel("Average Temperature [C]")
    plt.ylabel("Power [W]")

    plt.legend(loc="upper left", title="GPU Voltage Range")
    plt.show()


#f = pd.read_csv('/home/krisrst/thermal_test.csv')
#f = pd.read_csv('/home/krisrst/test.csv')
#f = pd.read_csv('/home/krisrst/test_results/leak_idle/3/all.csv')
f = pd.read_csv('/home/krisrst/test_results/leak_idle/2/all_nogpuv.csv')
#f = pd.read_csv('/home/krisrst/test_results/gpu_leak/2/all_combined.csv')


ones = np.ones(len(f['Timestamp']))
xs   = range(len(f['Timestamp']))

power = f['POWER']

"""
Platform voltages.
"""
hpv = f['VDD2']
gpuv = f['VDD1']
corev = f['VDD_CORE']
memv = ones * 1.1 # 1.1 V LPDDR4

"""
Square platform voltages.
"""
hpv2 = hpv * hpv
gpuv2 = gpuv * gpuv
corev2 = corev * corev
memv2 = memv * memv

"""
Leakage and Temperature
"""

temp_mem = f['TEMP_MEM']
temp_pll = f['TEMP_PLL']
temp_gpu = f['TEMP_GPU']
temp_cpu = f['TEMP_CPU']

temp_avg = (temp_mem + temp_pll + temp_gpu + temp_cpu) / 4

core0 = ones
core1 = f['CORE1']
core2 = f['CORE2']
core3 = f['CORE3']

#plt.plot(xs, temp_avg)

"""
Misc.
"""
cpu_cycles_tot = f['CPU0_CYCLES'] + f['CPU1_CYCLES'] + f['CPU2_CYCLES'] + f['CPU3_CYCLES']
cpu_inst_tot = f['CPU0_INST_EXEC_SW'] + f['CPU1_INST_EXEC_SW'] + f['CPU2_INST_EXEC_SW']+ f['CPU3_INST_EXEC_SW']
cpu_l1drf_tot = f['CPU0_L1D_CACHE_RF'] + f['CPU1_L1D_CACHE_RF'] + f['CPU2_L1D_CACHE_RF'] + f['CPU3_L1D_CACHE_RF']
cpu_l1irf_tot = f['CPU0_L1I_CACHE_RF'] + f['CPU1_L1I_CACHE_RF'] + f['CPU2_L1I_CACHE_RF'] + f['CPU3_L1I_CACHE_RF']
cpu_l2drf_tot = f['CPU0_L2D_CACHE_RF'] + f['CPU1_L2D_CACHE_RF'] + f['CPU2_L2D_CACHE_RF'] + f['CPU3_L2D_CACHE_RF']
cpu_l2dwb_tot = f['CPU0_L2D_CACHE_WB'] + f['CPU1_L2D_CACHE_WB'] + f['CPU2_L2D_CACHE_WB'] + f['CPU3_L2D_CACHE_WB']

com_l1l2 = (cpu_l1drf_tot - cpu_l2drf_tot) + cpu_l1irf_tot
com_l2ram = cpu_l2drf_tot

com_l1l2_p = com_l1l2 * hpv2
com_l2ram_p = com_l2ram * hpv2

cpu_inst_p = cpu_inst_tot * hpv2

mem_actmon_p = f['ACTMON_ALL'] * memv2

"""
Clock cycle predictors
"""
gpufrq_p = f['GPUFREQ'] * gpuv2
cpufrq_p = cpu_cycles_tot * hpv2
memfrq_p = f['MEMFREQ'] * memv2

#test = np.log(temp_avg)
#plt.scatter(test, power)
#plt.xlabel("SoC Temperature [C]")
#plt.ylabel("SMU Power [W]")

#ax = plt.gca()
#ax2 = ax.twinx()

#ax.plot(xs, temp_avg, color='r')
#ax2.plot(xs, power)



#fig = plt.figure()
#ax = fig.add_subplot(111, projection='3d')
#ax = Axes3D(fig)

#sc = ax.scatter(memfrq_p, temp_gpu, power, c=power, cmap='hot')
#sc = ax.scatter(gpuv, temp_avg, power, c=power, cmap='hot')
#fig.colorbar(sc, ax=ax)

#plot_grouped_volt( temp_avg, gpuv, power, num_groups=1)
plot_grouped_volt( temp_avg, corev, power, num_groups=5)

