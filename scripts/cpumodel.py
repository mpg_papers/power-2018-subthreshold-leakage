#! /usr/bin/python


# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
from sklearn import linear_model
import pandas as pd
import numpy  as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d, Axes3D
from numpy import linspace, meshgrid
from matplotlib.mlab import griddata
import matplotlib
from common_x1 import *
#import scipy.optimize as optimize
#from scipy.optimize import fsolve, root
from scipy.optimize import curve_fit

font = {'size'   : 10} # 30

matplotlib.rc('font', **font)

#f_leak = pd.read_csv('/home/krisrst/test_results/leak_idle/3/all_cpu_core_gpu.csv')
f_leak = pd.read_csv('/home/krisrst/test_results/leak_idle/3/all_combined_lessgpuvolt.csv')
f_range = pd.read_csv('/home/krisrst/test_results/leak_idle/freq_ranges/all_nogpuvolt.csv')
f_gpu = pd.read_csv('/home/krisrst/test_results/hppm/gpu/all_workloads_fixed_elapsed.csv')
f_cpu_int = pd.read_csv('/home/krisrst/test_results/hppm/cpu/int/all.csv')
f_cpu_neon = pd.read_csv('/home/krisrst/test_results/hppm/cpu/neon/all.csv')
f_cpu_vfp = pd.read_csv('/home/krisrst/test_results/hppm/cpu/vfp/all.csv')
f_cpu_int_v = pd.read_csv('/home/krisrst/test_results/hppm/cpu/int_vol/all.csv')
f_cpu_l1rf = pd.read_csv('/home/krisrst/test_results/hppm/cpu/l1rf/all.csv')
f_cpu_l1wb = pd.read_csv('/home/krisrst/test_results/hppm/cpu/l1wb/all.csv')

# Idle CPU inst
leak_inst = add_data( f_leak )
leak_range = add_data( f_range )
gpu_inst = add_data( f_gpu )

# CPU BMs
cpu_int  = add_data( f_cpu_int )
cpu_neon = add_data( f_cpu_neon )
cpu_vfp = add_data( f_cpu_vfp )
cpu_int_v = add_data( f_cpu_int_v )
cpu_l1rf = add_data( f_cpu_l1rf )
cpu_l1wb = add_data( f_cpu_l1wb )

# Sum up idle cpuinst
g_cpuinst_prof = [sum(x) for x in zip(g_cpuinst[2])]
c_cpuinst_idle = [sum(x) for x in zip(g_cpuinst[0], g_cpuinst[1])]
c_cpuinst_prof = [sum(x) for x in zip(g_cpuinst[3], g_cpuinst[4], g_cpuinst[5], g_cpuinst[6], g_cpuinst[7], g_cpuinst[8])]

"""
leak_gpu  = add_data( f_leak_gpu )
zeros = 0 * np.ones(len(leak_gpu) )
g_cpuinst_idle_gpu.extend( zeros )
g_cpuinst_idle_cpu.extend( zeros )
g_cpuinst_idle_core.extend( zeros )
#g_cpuinst_int.extend( zeros )

leak_core = add_data( f_leak_core )
zeros = 0 * np.ones(len(leak_core) )
g_cpuinst_idle_gpu.extend( zeros )
g_cpuinst_idle_cpu.extend( zeros )
g_cpuinst_idle_core.extend( zeros )
#g_cpuinst_int.extend( zeros )

leak_cpu  = add_data( f_leak_cpu )
zeros = 0 * np.ones(len(leak_cpu) )
g_cpuinst_idle_gpu.extend( zeros )
g_cpuinst_idle_cpu.extend( zeros )
g_cpuinst_idle_core.extend( zeros )
#g_cpuinst_int.extend( zeros )

#int_cpu = add_data( f_cpu_int )
#zeros = 0 * np.ones(len(int_cpu) )
#g_cpuinst_idle_gpu.extend( zeros )
#g_cpuinst_idle_cpu.extend( zeros )
#g_cpuinst_idle_core.extend( zeros )
#g_cpuinst_int.extend( int_cpu )
"""

# Initial guess
guess_KW_gpu = 5.0
guess_KW_core = 5.0
guess_KW_cpu = 5.0
guess_t_c_gpu = 2E-3
guess_t_c_core = 2E-3
guess_t_c_cpu = 2E-3
guess_base = 3.0
guess_cpu_load = 300E-10
guess_gpu_load = 200E-10
guess_mem_load = 200E-10
guess_l1l2 = 20E-9
guess_l2ram = 20E-9
guess_actmon = 2E-9
guess_inst = 0.2E-9
guess_nano = 1E-9
guess_pico = 1E-9
guess_gpu_act = 200E-3 # mW
                   
    #global g_cpu_l1drf_tot
    #global g_cpu_l1irf_tot
    #global g_cpu_l2drf_tot
    #global g_cpu_l2dwb_tot


"""

    g_gpu_int
    g_gpu_f32
    g_gpu_f64
    g_gpu_misc
    g_gpu_ctrl
    g_gpu_cnv
    g_gpu_l2r
    g_gpu_l2w
    g_gpu_l1
    g_gpu_ldst

"""

# Prepare curve fit
input_data      = [ g_temp_avg, g_gpuv, g_corev, g_hpv, 
                   g_cpufrq_p, g_gpufrq_p, g_memfrq_p,
                   g_com_l1l2_p, 
                   g_com_l2ram_p,
                   g_mem_actmon_p, 
                   
                   g_cpuinst_prof, c_cpuinst_idle,

                   c_cpuinst_prof,
                   
                   #g_cpuinst[3], 
                   g_cpuinst[4], g_cpuinst[5], 
                   g_cpuinst[6], g_cpuinst[7], g_cpuinst[8],
                   
                   g_gpu_ccl,                       
                   g_gpu_int,
                   g_gpu_f32,
                   g_gpu_f64,
                   g_gpu_misc,
                   g_gpu_ctrl,
                   g_gpu_cnv,
                   g_gpu_l2r,
                   g_gpu_l2w,
                   g_gpu_l1,
                   g_gpu_ldst,
                   g_cores,
                   g_gpu_f16,
                   g_gpu_shr,
                   g_gpu_shw,
                   g_gpu_act
                   ]

names = ["p_kw_gpu", "p_t_c", "p_base_power", 
                   "p_kw_core", #"guess_t_c", 
                   "p_kw_percpu", #"guess_t_c", 
                   "p_cpu_cycles", 
                   "p_gpu_cycles", 
                   "p_mem_cycles",
                   'p_com_l1l2', 
                   'p_guess_l2ram',
                   "p_actmon_act",
                   
                   "g_cpuinst_prof", 
                   "c_cpuinst_idle",
                   "p_cpu_int",
                   "p_cpu_neon",
                   "p_cpu_vfp",
                   "p_cpu_int_vol",
                   "p_cpu_l1rf",
                   "p_cpu_l1wb",

                   "p_gpu_ccl",
                   "p_gpu_int",
                   "p_gpu_f32",
                   "p_gpu_f64",
                   "p_gpu_misc",
                   "p_gpu_ctrl",
                   "p_gpu_cnv",
                   "p_gpu_l2r",
                   "p_gpu_l2w",
                   "p_gpu_l1",
                   "p_gpu_ldst",
                   "p_gpu_f16",
                   "p_gpu_shr",
                   "p_gpu_shw",
                   "p_gpu_act"
                   ]
#initial_guess = []
#for e in names:
#    cmd = "coeffs.append( %s )" % e
#    exec(cmd)

initial_guess   = [ guess_KW_gpu, guess_t_c_gpu, guess_base, 
                   guess_KW_core, #guess_t_c_core, 
                   guess_KW_cpu,  #guess_t_c_cpu, 
                   guess_cpu_load, 
                   guess_gpu_load, 
                   guess_mem_load,
                   guess_l1l2, 
                   guess_l2ram, 
                   guess_actmon,
                   guess_inst, guess_inst,
                   guess_inst, guess_inst, guess_inst, 
                   guess_inst, guess_inst, guess_inst,
                   guess_pico,
                   guess_pico,
                   guess_pico,
                   guess_pico,
                   guess_pico,
                   guess_pico,
                   guess_pico,
                   guess_nano,
                   guess_nano,
                   guess_nano,
                   guess_pico,
                   guess_pico,
                   guess_pico,
                   guess_pico,
                   guess_gpu_act
                   ]    

#popt, pcov = curve_fit(func_hppm, input_data, g_power, initial_guess, method='lm')

#for x in zip(names, popt):
#    print "%s = %s" % (x[0], x[1])
    
min_vec = np.array( -1 )
s_t = g_temp_avg
t_coeff = 0.00307173452517


KW = 21.0252784758
core_KW = 6.96750151714
rest_cpu_KW = 6.10171589898
s_cores = g_cores
s_gpuv = g_gpuv
s_corev = g_corev
s_cpuv = g_hpv

gpu_e1 = np.exp( np.array((min_vec * v_thresh_gpu )   / func_temp( s_t, t_coeff )))
gpu_e2 = np.exp( np.array((min_vec * s_gpuv )         / func_temp( s_t, t_coeff )))

core_e1 = np.exp( np.array((min_vec * v_thresh_core ) / func_temp( s_t, t_coeff )))
core_e2 = np.exp( np.array((min_vec * s_corev )       / func_temp( s_t, t_coeff )))

cpu_e1 = np.exp( np.array((min_vec * v_thresh_cpu )   / func_temp( s_t, t_coeff )))
cpu_e2 = np.exp( np.array((min_vec * s_cpuv )         / func_temp( s_t, t_coeff )))


g_power = g_power - np.array(1.59702270641)

g_power = g_power - ((core_e1 - ( core_e2 * core_e1 ) ) * core_KW * s_corev) * 1
g_power = g_power - ((gpu_e1 - ( gpu_e2 * gpu_e1 ) ) * KW * s_gpuv)
g_power = g_power - ((cpu_e1 - ( cpu_e2 * cpu_e1 ) ) * rest_cpu_KW * s_cores * s_cpuv)


clf = linear_model.LinearRegression(fit_intercept=False)

input_regress      = np.array([ g_cpufrq_p, g_gpufrq_p, g_memfrq_p,
                   g_com_l1l2_p, 
                   #g_com_l2ram_p,
                   g_mem_actmon_p, 
                   c_cpuinst_prof,
                   
                   #g_gpu_ccl,                       
                   g_gpu_int,
                   g_gpu_f32,
                   g_gpu_f64,
                   g_gpu_misc,
                   g_gpu_ctrl,
                   g_gpu_cnv,
                   g_gpu_l2r,
                   #g_gpu_l2w,
                   g_gpu_l1,
                   #g_gpu_ldst,
                   g_gpu_f16,
#                   g_gpu_shr,
                   g_gpu_shw
                   ])

new_test = []
for i in range(len(g_power)):

    new_test.append([])
    for e in input_regress:        
        
        new_test[i].append(e[i])
    

clf.fit(new_test, g_power)

print clf.coef_



















