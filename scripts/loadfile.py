#! /usr/bin/python


# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import pandas as pd
import numpy  as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d, Axes3D
from numpy import linspace, meshgrid
from matplotlib.mlab import griddata
import matplotlib
#import scipy.optimize as optimize
#from scipy.optimize import fsolve, root
from scipy.optimize import curve_fit

font = {'size'   : 30} # 30

matplotlib.rc('font', **font)

temp_t20 = 25.0E-3
temp_coeff = 3.0E-3
def func_temp( temp, coeff ):
    
    _d = ((temp * coeff) + temp_t20)
    return _d


g_temp_avg = []
g_hpv = []
g_corev = []
g_gpuv = []
g_memv = []
g_cpufrq_p = []
g_gpufrq_p = []
g_memfrq_p = []
g_com_l1l2_p = []
g_com_l2ram_p = []
g_mem_actmon_p = []
g_power = []
g_cpuinst_idle_cpu = []
g_cpuinst_idle_gpu = []
g_cpuinst_idle_core = []
g_cpuinst_int = []

def add_data( f ):
    
    global g_temp_avg
    global g_hpv
    global g_corev
    global g_gpuv
    global g_memv
    global g_cpufrq_p
    global g_gpufrq_p
    global g_memfrq_p
    global g_com_l1l2_p
    global g_com_l2ram_p
    global g_mem_actmon_p

    ones = np.ones(len(f['Timestamp']))
    xs   = range(len(f['Timestamp']))
    dur = f['Duration'] / 1E3
    ind = np.linspace(0, len(xs), num=len(xs))
    
    power = f['POWER']
    g_power.extend( power )
    
    """
    Platform voltages.
    """
    hpv = f['VDD2']
    gpuv = f['VDD1']
    corev = f['VDD_CORE']
    memv = ones * 1.1 # 1.1 V LPDDR4
    
    g_hpv.extend( hpv )
    g_corev.extend( corev )
    g_gpuv.extend( gpuv )
    g_memv.extend( memv )
    
    """
    Square platform voltages.
    """
    hpv2 = hpv * hpv
    gpuv2 = gpuv * gpuv
    corev2 = corev * corev
    memv2 = memv * memv
    
    """
    Leakage and Temperature
    """
    
    temp_mem = f['TEMP_MEM']
    temp_pll = f['TEMP_PLL']
    temp_gpu = f['TEMP_GPU']
    temp_cpu = f['TEMP_CPU']
    
    temp_avg = (temp_mem + temp_pll + temp_gpu + temp_cpu) / 4
    g_temp_avg.extend( temp_avg )
    
    core0 = ones
    core1 = f['CORE1']
    core2 = f['CORE2']
    core3 = f['CORE3']
    
    #plt.plot(xs, temp_avg)
    
    """
    Misc.
    """
    cpu_cycles_tot = f['CPU0_CYCLES'] + f['CPU1_CYCLES'] + f['CPU2_CYCLES'] + f['CPU3_CYCLES']
    cpu_inst_tot = f['CPU0_INST_EXEC_SW'] + f['CPU1_INST_EXEC_SW'] + f['CPU2_INST_EXEC_SW']+ f['CPU3_INST_EXEC_SW']
    cpu_l1drf_tot = f['CPU0_L1D_CACHE_RF'] + f['CPU1_L1D_CACHE_RF'] + f['CPU2_L1D_CACHE_RF'] + f['CPU3_L1D_CACHE_RF']
    cpu_l1irf_tot = f['CPU0_L1I_CACHE_RF'] + f['CPU1_L1I_CACHE_RF'] + f['CPU2_L1I_CACHE_RF'] + f['CPU3_L1I_CACHE_RF']
    cpu_l2drf_tot = f['CPU0_L2D_CACHE_RF'] + f['CPU1_L2D_CACHE_RF'] + f['CPU2_L2D_CACHE_RF'] + f['CPU3_L2D_CACHE_RF']
    cpu_l2dwb_tot = f['CPU0_L2D_CACHE_WB'] + f['CPU1_L2D_CACHE_WB'] + f['CPU2_L2D_CACHE_WB'] + f['CPU3_L2D_CACHE_WB']
    
    com_l1l2 = (cpu_l1drf_tot - cpu_l2drf_tot) + cpu_l1irf_tot
    com_l2ram = cpu_l2drf_tot

    com_l1l2_p = com_l1l2 * hpv2
    com_l2ram_p = com_l2ram * hpv2
    g_com_l1l2_p.extend( com_l1l2_p / dur )
    g_com_l2ram_p.extend( com_l2ram_p / dur )
    
    cpu_inst_p = cpu_inst_tot * hpv2  / dur
    
    mem_actmon_p = f['ACTMON_ALL'] * memv2
    g_mem_actmon_p.extend( mem_actmon_p / dur )
    
    """
    Clock cycle predictors
    """
    gpufrq = f['GPUFREQ']
    gpufrq_p = f['GPUFREQ'] * gpuv2 * 1E3
    cpufrq_p = cpu_cycles_tot * hpv2
    memfrq = f['MEMFREQ']
    memfrq_p = f['MEMFREQ'] * memv2 * 1E3
    
    g_cpufrq_p.extend(cpufrq_p / dur)
    g_gpufrq_p.extend(gpufrq_p)
    g_memfrq_p.extend(memfrq_p)
    
    """
    Plot
    """
    fig = plt.figure()
    ax = fig.add_subplot(111)
    ax.scatter(gpufrq_p, power)
    #ax2 = ax.twinx()
    #ax2.scatter(gpufrq_p, temp_avg, color='r')
    #ax2.set_ylim([0, 70])
    
    """
    Return workload-specific
    """
    return cpu_inst_p
    
v_thresh_cpu = 0.8
v_thresh_gpu = 0.8
v_thresh_core = 0.8

def func_leak( input_data, 
              KW, t_coeff, base, core_KW, core_t_coeff, rest_cpu_KW, rest_cpu_t_coeff,
              cpu_load, 
              gpu_load, 
              mem_load,
                   #l1l2, l2ram, 
              actmon,
              inst, inst2
                   ):
    
    min_vec = ( -1 )
    
    s_t = input_data[0]
    s_gpuv = input_data[1]
    s_corev = input_data[2]
    s_cpuv = input_data[3]
    s_cpuccl = input_data[4]
    s_gpuccl = input_data[5]
    s_memccl = input_data[6]
    #s_l1l2   = input_data[7]
    #s_l2ram   = input_data[8]
    s_actmon   = input_data[7]
    s_cpuinst = input_data[8]
    s_cpuinst2 = input_data[9]
    
    gpu_e1 = np.exp( (min_vec * v_thresh_gpu )   / func_temp( s_t, t_coeff ))
    gpu_e2 = np.exp( (min_vec * s_gpuv )         / func_temp( s_t, t_coeff ))
    
    core_e1 = np.exp( (min_vec * v_thresh_core ) / func_temp( s_t, core_t_coeff ))
    core_e2 = np.exp( (min_vec * s_corev )       / func_temp( s_t, core_t_coeff ))
    
    cpu_e1 = np.exp( (min_vec * v_thresh_cpu )   / func_temp( s_t, rest_cpu_t_coeff ))
    cpu_e2 = np.exp( (min_vec * s_cpuv )         / func_temp( s_t, rest_cpu_t_coeff ))
    
    res = base
    res = res + ((core_e1 - ( core_e2 * core_e1 ) ) * core_KW * s_corev)
    res = res + ((gpu_e1 - ( gpu_e2 * gpu_e1 ) ) * KW * s_gpuv)
    res = res + ((cpu_e1 - ( cpu_e2 * cpu_e1 ) ) * rest_cpu_KW * s_cpuv)
    res = res + (s_cpuccl * cpu_load)
    res = res + (s_gpuccl * gpu_load)
    res = res + (s_memccl * mem_load)
    #res = res + (s_l1l2 * l1l2)
    #res = res + (s_l2ram * l2ram)
    res = res + (s_actmon * actmon)
    res = res + (s_cpuinst * inst)
    res = res + (s_cpuinst2 * inst2)
    
    return res

#f_leak = pd.read_csv('/home/krisrst/test_results/leak_idle/3/all_cpu_core_gpu.csv')
f_range = pd.read_csv('/home/krisrst/test_results/leak_idle/freq_ranges/all_vary_only_gpuf.csv')
#f_leak_gpu  = pd.read_csv('/home/krisrst/test_results/leak_idle/1/all.csv')
#f_leak_core = pd.read_csv('/home/krisrst/test_results/leak_idle/2/all.csv')
#f_leak_cpu  = pd.read_csv('/home/krisrst/test_results/leak_idle/3/all.csv')
#f_cpu_int = pd.read_csv('/home/krisrst/test_results/hppm/cpu/int/all.csv')



inst = add_data( f_range )